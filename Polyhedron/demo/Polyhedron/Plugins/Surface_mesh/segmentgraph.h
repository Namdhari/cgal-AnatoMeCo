#ifndef SEGMENTGRAPH_H
#define SEGMENTGRAPH_H

//includes
#include <boost/config.hpp>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <boost/graph/adjacency_list.hpp>
#include <vector>
#include "hungarian.h"
#include "SDF_Signature.h"
#include <stdio.h>
#include <math.h>
#include "emd.h"


#define LOG



/*Enable type normalization needed*/
//#define LOCAL_NORMALIZATION
//#define GLOBAL_LINEAR_NORMALIZATION
#define GLOBAL_NON_LINEAR_NORMALIZATION

//constants
//#define MAX_SEGMENTS 15
const double pi = 3.141592653589793238462643;



//Jus taking it to make some implementation easy, I dont think no. of degrees could exceed this.
#define MAX_VERTEX_DEGREE 4



//Atributes assigned to edges
struct BinaryAttributes
{
    double segment_euclidean_distance;
    double angle_between_max_eigenvector;
    double angle_between_second_max_eigenvector;
};


//Atributes assigned to vertices
struct UnaryAttributes
{
    //Name of that segment
    std::string segment_name;

    //Ar(segment)/Ar(mesh)
    double segment_area_ratio;

    //Centroid of Polyseg
    Eigen::Vector3f segcentroid;

    //Eigen Vectors of Segment
    Eigen::Matrix3d EigenVectors;

    //lambda_max/lambda_second_max of segment
    double eigen_value_ratio;

    //SI Value Histogram of segment
     std::vector<double> si_signature;

     bool IsMatched;
     int vert_index;
     int  matchedToIndex;
     std::string matchedTo;


     // Eigen Shape
     /*Based on Unsupervised Co-segmentation : Sidi and Zhangh */
     std::vector<double> eigen_shape;

    //SDF Histogram of segment ??
     std::vector<double> sdf_signature;

     Polyhedron polyseg;

};


struct DistanceMeasures
{
    double segment_area_ratio_diff;
    float emd_si_sig;
    float emd_sdf_sig;
    double eu_eigen_shape_descr;

};

typedef boost::adjacency_list<boost::setS, boost::vecS,boost::undirectedS,UnaryAttributes,BinaryAttributes> AttrRelGraph;


//extern int out_PermMat[MAX_SEGMENTS][MAX_SEGMENTS];


//API's

AttrRelGraph CreateSegmentARG(int adjmat[][MAX_SEGMENTS], int vertex_count);

double ComputeOptimumDistanceForBinaryAttribute(std::vector<std::pair<double, double> > Sig1, std::vector<std::pair<double, double> > Sig2);

void ComputeCostMatrixForARGS(AttrRelGraph myARG_groundtruth,AttrRelGraph myARG_testmesh);

void ComputePermutationMatrixForARGS(Eigen::MatrixXf CostMat, Eigen::MatrixXd &PermMat, int rawPermMat[][MAX_SEGMENTS] );

double ComputePartBasedMetric(const Eigen::MatrixXf &CostMat,const Eigen::MatrixXd &PermMat,int segment_diff);

double ComputeImprovedPartBasedMetric(const Eigen::MatrixXf &CostMat,const Eigen::MatrixXd &PermMat,int segment_diff);

void ComputeSegmentMetric();

#ifdef LOCAL_NORMALIZATION
//Normalizes for a particular segment-signature
Eigen::MatrixXf ComputeDistanceForUnaryAttribute(AttrRelGraph myARGA, AttrRelGraph myARGB);
#endif

#ifdef GLOBAL_LINEAR_NORMALIZATION
//Linearly Normalizes across all the segments for a particular signature
Eigen::MatrixXf ComputeDistanceForUnaryAttribute2(AttrRelGraph myARGA, AttrRelGraph myARGB);
#endif

#ifdef GLOBAL_NON_LINEAR_NORMALIZATION
//Log Normalizes across all the segments for a particular signature
Eigen::MatrixXf ComputeDistanceForUnaryAttribute3(AttrRelGraph myARGA, AttrRelGraph myARGB);
#endif

void PopulateUnaryProps(AttrRelGraph &myARG, std::vector < UnaryAttributes > props);
void PopulateBinaryProps(AttrRelGraph &myARG);

float dist(feature_t *F1, feature_t *F2);

float ComputeEMDForSegmentSignatures(std::vector<double> Sig1, std::vector<double> Sig2);

double ComputeEuclideanDistance(std::vector<double> Sig1, std::vector<double> Sig2);

double ComputeEuclideanDistanceForEigenSig(std::vector<double> Sig1, std::vector<double> Sig2);

std::vector< std::pair<double,double> > GetEigenSignature(boost::graph_traits<AttrRelGraph>::vertex_descriptor curr_vert, const AttrRelGraph &myARG, int max_degree);

void UpdatePolysegAssignment(AttrRelGraph &myARGA, AttrRelGraph &myARGB, int rawPermMat[][MAX_SEGMENTS]);

void GetNeighBour( boost::graph_traits<AttrRelGraph>::out_edge_iterator ei, const AttrRelGraph &myARG);

bool CompareDistances(UnaryAttributes propsA,UnaryAttributes propsB,DistanceMeasures &dist);

//Test Functions
void TestSegArg(AttrRelGraph myARG);
void TestLibHungarian();
void PrintSDFSignature(std::vector<double> hist, std::string name);
void PrintSISignature(std::vector<double> hist,std::string name);
void PrintMatching(AttrRelGraph myARG);



#endif // SEGMENTGRAPH_H
