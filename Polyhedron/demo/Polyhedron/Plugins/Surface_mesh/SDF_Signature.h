#ifndef SDFSIGNATURE_H
#define SDFSIGNATURE_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/mesh_segmentation.h>

#include <CGAL/Polygon_mesh_processing/measure.h>
#include "Polyhedron_type.h"


#include <CGAL/property_map.h>

#include <iostream>
#include <fstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/density.hpp>
#include <boost/accumulators/statistics/stats.hpp>



typedef boost::accumulators::accumulator_set<double, boost::accumulators::features<boost::accumulators::tag::density> > acc;
typedef boost::iterator_range<std::vector<std::pair<double, double> >::iterator > histogram_type;


std::vector<double> ComputeSDFSignature(Polyhedron &polyhedron);


#endif
