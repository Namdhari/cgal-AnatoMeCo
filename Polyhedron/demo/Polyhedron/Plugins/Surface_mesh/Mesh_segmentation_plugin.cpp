#include <CGAL/Three/Polyhedron_demo_plugin_helper.h>
#include <CGAL/Three/Polyhedron_demo_plugin_interface.h>




#include "ui_Mesh_segmentation_widget.h"
#include "Scene_polyhedron_item.h"
#include "Polyhedron_type.h"
#include "Scene.h"
#include "Color_map.h"

#include "segmentgraph.h"
#include "SDF_Signature.h"



#include <CGAL/mesh_segmentation.h>
#include <QApplication>
#include <QMainWindow>
#include <QInputDialog>
#include <QTime>
#include <QAction>
#include <QDebug>
#include <QObject>
#include <QDockWidget>
//#include <QtConcurrentRun>
#include <map>
#include <algorithm>
#include <vector>
#include <limits>
#include <CGAL/property_map.h>


//For Segment Builder
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Polyhedron_3.h>
#include<CGAL/IO/Polyhedron_iostream.h>
//For reading polygons
#include <fstream>
//For PCA
#include <CGAL/linear_least_squares_fitting_3.h>

//For SI
#include <CGAL/Monge_via_jet_fitting.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>


//For Batch Processing Comparison
#include <CGAL/IO/OFF_reader.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>

#include<dirent.h>
struct ResultInfo
{
    std::string polyname_src;
    std::string polyname_trg;
    float si_sig_diff;
    float ci_sig_diff;
    float sdf_sig_diff;
    float gaussianc_sig_diff;
    float meanc_sig_diff;
    float eigen_shape_sig_diff;
    float improved_global_metric;
};


template<class PolyhedronWithId, class ValueType>
struct Polyhedron_with_id_to_vector_property_map
    : public boost::put_get_helper<ValueType&,
             Polyhedron_with_id_to_vector_property_map<PolyhedronWithId, ValueType> >
{
public:
    typedef typename PolyhedronWithId::Facet_const_handle key_type;
    typedef ValueType value_type;
    typedef value_type& reference;
    typedef boost::lvalue_property_map_tag category;

    Polyhedron_with_id_to_vector_property_map() : internal_vector(NULL) { }
    Polyhedron_with_id_to_vector_property_map(std::vector<ValueType>* internal_vector)
         : internal_vector(internal_vector) { }
        
    reference operator[](key_type key) const { return (*internal_vector)[key->id()]; }
private:
    std::vector<ValueType>* internal_vector;
};

template<class PolyhedronWithId, class ValueType>
struct Polyhedron_with_id_to_vector_of_vertices_property_map
    : public boost::put_get_helper<ValueType&,
             Polyhedron_with_id_to_vector_of_vertices_property_map<PolyhedronWithId, ValueType> >
{
public:
    typedef typename PolyhedronWithId::Vertex_const_handle key_type;
    typedef ValueType value_type;
    typedef value_type& reference;
    typedef boost::lvalue_property_map_tag category;

    Polyhedron_with_id_to_vector_of_vertices_property_map() : internal_vector(NULL) { }
    Polyhedron_with_id_to_vector_of_vertices_property_map(std::vector<ValueType>* internal_vector)
         : internal_vector(internal_vector) { }
        
    reference operator[](key_type key) const { return (*internal_vector)[key->id()]; }
private:
    std::vector<ValueType>* internal_vector;
};
using namespace CGAL::Three;
class Polyhedron_demo_mesh_segmentation_plugin : 
    public QObject,
    public Polyhedron_demo_plugin_helper
{
    Q_OBJECT
    Q_INTERFACES(CGAL::Three::Polyhedron_demo_plugin_interface)
    Q_PLUGIN_METADATA(IID "com.geometryfactory.PolyhedronDemo.PluginInterface/1.0")

private:
    typedef std::map<Scene_polyhedron_item*, std::vector<double> > Item_sdf_map;
    typedef std::map<Scene_polyhedron_item*, std::vector<double> > Item_SI_map;
    double areaMeshA;
    double areaMeshB;
    AttrRelGraph myGraphA;
    AttrRelGraph myGraphB;
    std::vector <Polyhedron> PolySegmentsA;
    std::vector <Polyhedron> PolySegmentsB;
    std::vector <UnaryAttributes> propsA;
    std::vector <UnaryAttributes> propsB;

public:

    QList<QAction*> actions() const {
        return QList<QAction*>() << actionSegmentation;
    }

    bool applicable(QAction*) const {
      return 
        qobject_cast<Scene_polyhedron_item*>(scene->item(scene->mainSelectionIndex()));
    }    
    
    void init(QMainWindow* mainWindow, CGAL::Three::Scene_interface* scene_interface) {
        this->scene = scene_interface;
        this->mw = mainWindow;
        actionSegmentation = new QAction("AnatoMeco", mw);
        actionSegmentation->setProperty("subMenuName", "Anatomical Mesh Comparison");
        connect(actionSegmentation, SIGNAL(triggered()),this, SLOT(on_actionSegmentation_triggered()));

        // adding slot for itemAboutToBeDestroyed signal, aim is removing item from item-functor map.
        
        if( Scene* scene = dynamic_cast<Scene*>(scene_interface) ) {
            connect(scene, SIGNAL(itemAboutToBeDestroyed(CGAL::Three::Scene_item*)), this, SLOT(itemAboutToBeDestroyed(CGAL::Three::Scene_item*)));
        }
        
        init_color_map_sdf();
        init_color_map_segmentation();
        init_color_map_SI();

        dock_widget = new QDockWidget("Mesh segmentation parameters", mw);
        dock_widget->setVisible(false); // do not show at the beginning
        ui_widget.setupUi(dock_widget);
        mw->addDockWidget(Qt::LeftDockWidgetArea, dock_widget);
    
        connect(ui_widget.Partition_button,  SIGNAL(clicked()), this, SLOT(on_CompareModels_clicked()));
        connect(ui_widget.refineSegments_Button,  SIGNAL(clicked()), this, SLOT(on_RefineCompareModels_clicked()));
        connect(ui_widget.SDF_button,  SIGNAL(clicked()), this, SLOT(on_SDF_button_clicked()));   
        connect(ui_widget.SI_button,  SIGNAL(clicked()), this, SLOT(on_SI_button_clicked()));
        connect(ui_widget.batchProcess_Button,  SIGNAL(clicked()), this, SLOT(OnBatchCompare()));
    }
    virtual void closure()
    {
      dock_widget->hide();
    }
    
    template<class SDFPropertyMap>
    void colorize_sdf(Scene_polyhedron_item* item, SDFPropertyMap sdf_values, std::vector<QColor>& color_vector);
    template<class SegmentPropertyMap> 
    void colorize_segmentation(Scene_polyhedron_item* item, SegmentPropertyMap segment_ids, std::vector<QColor>& color_vector);

    /* API's related to graph from segments */
    template<class SegmentPropertyMap>
    void GetSegments(Scene_polyhedron_item* item, SegmentPropertyMap segment_ids,
                     int nb_segments,std::vector <Polyhedron> &PolySegments,int index);

    template<class SegmentPropertyMap>
    void CreateAdjMat( Scene_polyhedron_item* item,SegmentPropertyMap segment_ids,
                       int adjmat[][MAX_SEGMENTS]);

    //Build Segments
    int BuildSegments(std::vector<double> &coords,std::vector<int> &tris,Polyhedron &P);
    void ClearGraph();

    //Fore refining segmentation
    void MergeSegments(Polyhedron &PolySeg1, Polyhedron &PolySeg2, std::string name, Polyhedron &MergedPoly);
    void ComputeUnaryProps(Polyhedron &PolyMesh, UnaryAttributes &props, std::string polysegname, double fullMeshArea);
    bool GetDecisionLogical(DistanceMeasures distOrig,DistanceMeasures distMerged);
    void contract_vertices(boost::graph_traits<AttrRelGraph>::vertex_descriptor b,
                           boost::graph_traits<AttrRelGraph>::vertex_descriptor a,
                           AttrRelGraph& myGraph);
    void ReComputeProps(AttrRelGraph &myGraph);
    bool ReconfigureGraph(AttrRelGraph &myGraph,std::vector< std::pair<int,int> > &mergedSegIDs );
    void SavePolysegs(AttrRelGraph &myGraph);

    //Batch Processing related
    std::vector <std::string> TraveseDir(std::string dir_path);
    int ReadPolyhedron(const char *path, Polyhedron& P);
    void SaveResults(std::vector<ResultInfo> &results);







    //Prinf the adjacency matrix
    void PrintAdjMat(int adjmat[][MAX_SEGMENTS],int dimension);

    std::vector<double> ComputeEigenShape(EigenAnalysis eigenanalysis);

    //Compute the SI Signature of segment
    std::vector<double> ComputeSISignature(Polyhedron &polyhedron);

    std::vector<double> ComputeCISignature(Polyhedron &polyhedron);


    std::vector<double> ComputeGaussianCurvatureSignature(Polyhedron &polyhedron);
    std::vector<double> ComputeMeanCurvatureSignature(Polyhedron &polyhedron);

    void ComputeEigenAnalysis(Polyhedron &polyhedron,EigenAnalysis &eigenanalysis,double *centroid);
    double GetSurfaceArea(int index);
    std::vector<double> ComputeGlobalMetric(int indexA, int indexB);

    template<class SIPropertyMap>
    void colorize_si(Scene_polyhedron_item* item, SIPropertyMap si_values, std::vector<QColor>& color_vector);
    void check_and_set_ids(Polyhedron* polyhedron);
    void check_and_set_vertex_ids(Polyhedron* polyhedron);
    void init_color_map_sdf();
    void init_color_map_segmentation();
    void init_color_map_SI();
    void on_Partition_button_clicked(int index, std::vector<Polyhedron> &PolySegments, int adjmat[][MAX_SEGMENTS]);
    public Q_SLOTS:
        void on_actionSegmentation_triggered();
        void on_CompareModels_clicked();
        void on_SDF_button_clicked();
        void on_SI_button_clicked();
        void on_RefineCompareModels_clicked();
        void itemAboutToBeDestroyed(CGAL::Three::Scene_item*);
         void OnBatchCompare();
private:
    template<class SIPropertyMap>
    void  si_values(Polyhedron& polyhedron,SIPropertyMap& si_values_map);
    QAction*                      actionSegmentation;
    QDockWidget*                  dock_widget;
    Ui::Mesh_segmentation         ui_widget;
    
    std::vector<QColor>  color_map_sdf;
    std::vector<QColor>  color_map_segmentation;

    //For Coloring of SI values
    std::vector<QColor>  color_map_SI;

    Item_sdf_map         item_sdf_map;
    Item_SI_map          item_SI_map;
};

void Polyhedron_demo_mesh_segmentation_plugin::init_color_map_sdf()
{
    color_map_sdf = std::vector<QColor>(256);
    int r = 0, g = 0, b = 255;
    for(int i = 0; i <= 255; ++i)
    {
        if(i > 128 && i <= 192) { r = static_cast<int>( ((i - 128) / (192.0 - 128)) * 255 ); }        
        if(i > 0 && i <= 98)    { g = static_cast<int>( ((i) / (98.0)) * 255 ); }
        if(i > 191 && i <=255)  { g = 255 - static_cast<int>( ((i - 191) / (255.0 - 191)) * 255 ); }
        if(i > 64 && i <= 127)  { b = 255 - static_cast<int>( ((i - 64) / (127.0 - 64)) * 255 ); }
        color_map_sdf[i] = QColor(r, g, b);        
    }
}

void Polyhedron_demo_mesh_segmentation_plugin::init_color_map_segmentation()
{

    color_map_segmentation.push_back(QColor( 173, 35, 35)); 
    color_map_segmentation.push_back(QColor( 87, 87, 87));    
    color_map_segmentation.push_back(QColor( 42, 75, 215)); 
    color_map_segmentation.push_back(QColor( 29, 105, 20)); 
    color_map_segmentation.push_back(QColor( 129, 74, 25)); 
    color_map_segmentation.push_back(QColor( 129, 38, 192)); 
    color_map_segmentation.push_back(QColor( 160, 160, 160)); 
    color_map_segmentation.push_back(QColor( 129, 197, 122)); 
    color_map_segmentation.push_back(QColor( 157, 175, 255)); 
    color_map_segmentation.push_back(QColor( 41, 208, 208)); 
    color_map_segmentation.push_back(QColor( 255, 146, 51)); 
    color_map_segmentation.push_back(QColor( 255, 238, 51)); 
    color_map_segmentation.push_back(QColor( 233, 222, 187)); 
    color_map_segmentation.push_back(QColor( 255, 205, 243)); 
    
}


void Polyhedron_demo_mesh_segmentation_plugin::init_color_map_SI()
{
    color_map_SI.push_back(QColor(   0, 255,   0)); //Green:Spherical Cup 
    color_map_SI.push_back(QColor(   0, 255, 127)); //Cyan:Trough   
    color_map_SI.push_back(QColor(   0, 255, 255)); //Blue:Rut
    color_map_SI.push_back(QColor( 127, 255, 255)); //Pale Blue: Saddle rut
    color_map_SI.push_back(QColor( 255, 255, 255)); //White:Saddle
    color_map_SI.push_back(QColor( 255, 255, 127)); //Pale Yellow:Saddle ridge
    color_map_SI.push_back(QColor( 255, 255,   0)); //Yellow:Ridge 
    color_map_SI.push_back(QColor( 255, 127,   0)); //Orange:Dome
    color_map_SI.push_back(QColor( 255,   0,   0)); //Red:Spherical cap
    
}
void Polyhedron_demo_mesh_segmentation_plugin::itemAboutToBeDestroyed(CGAL::Three::Scene_item* scene_item)
{
    if(Scene_polyhedron_item* item = qobject_cast<Scene_polyhedron_item*>(scene_item)) {
      item_sdf_map.erase(item);
      item_SI_map.erase(item);
    }
}

void Polyhedron_demo_mesh_segmentation_plugin::on_actionSegmentation_triggered()
{ dock_widget->show(); }



void Polyhedron_demo_mesh_segmentation_plugin::on_SI_button_clicked()
{
    CGAL::Three::Scene_interface::Item_id index = scene->mainSelectionIndex();
    Scene_polyhedron_item* item = qobject_cast<Scene_polyhedron_item*>(scene->item(index));
    if(!item) { return; }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    bool create_new_item = ui_widget.New_item_check_box->isChecked();
    
    Item_SI_map::iterator pair;
    Scene_polyhedron_item* active_item = item;

    if(create_new_item) {
        active_item = new Scene_polyhedron_item(*item->polyhedron()); 
        active_item->setGouraudMode();
    }
    
    pair = item_SI_map.insert(
            std::make_pair(active_item, std::vector<double>()) ).first; 
    
    check_and_set_vertex_ids(pair->first->polyhedron());
    pair->second.resize(item->polyhedron()->size_of_vertices(), 0.0);
    Polyhedron_with_id_to_vector_of_vertices_property_map<Polyhedron, double>  SI_pmap(&pair->second);
    QTime time;
    time.start();
    si_values(*(pair->first->polyhedron()), SI_pmap);
    std::cout << "ok (" << time.elapsed() << " ms)" << std::endl;


    pair->first->set_color_vector_read_only(true);
    colorize_si(pair->first, SI_pmap, pair->first->color_vector());
   // pair->first->setName(tr("(SDF-%1-%2)").arg(number_of_rays).arg(ui_widget.Cone_angle_spin_box->value()));
    
    if(create_new_item) {
        scene->addItem(pair->first);
        item->setVisible(false);
        scene->itemChanged(item);
        scene->itemChanged(pair->first);
        scene->setSelectedItem(index);
    }
    else {
      item->invalidate_buffers();
      scene->itemChanged(index);
    }

    QApplication::restoreOverrideCursor();
}
template<class SIPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::si_values(Polyhedron& polyhedron,SIPropertyMap& si_values_map)
{
	// types
      typedef CGAL::Monge_via_jet_fitting<Kernel> Fitting;
	  typedef Fitting::Monge_form Monge_form;

	  typedef Kernel::Point_3 Point;

	  Polyhedron::Vertex_iterator v;
	  for(v = polyhedron.vertices_begin();
	      v != polyhedron.vertices_end();
	      v++)
	  {
	    std::vector<Point> points;

	    // pick central point
	    const Point& central_point = v->point();
	    points.push_back(central_point);

	   	Polyhedron::Halfedge_around_vertex_circulator he = v->vertex_begin();
	    Polyhedron::Halfedge_around_vertex_circulator end = he;


	    CGAL_For_all(he,end)
	    {
	      const Point& p = he->opposite()->vertex()->point();
	      points.push_back(p);
	    }

	    if(points.size() > 5)
	    {
	      // estimate curvature by fitting
	      Fitting monge_fit;
	      const int dim_monge = 2;
	      const int dim_fitting = 2;
	      Monge_form monge_form = monge_fit(points.begin(),points.end(),dim_fitting,dim_monge);

	      // make monge form comply with vertex normal (to get correct
	      // orientation)
	      typedef Kernel::Vector_3 Vector;
	      Vector n = CGAL::Polygon_mesh_processing::compute_vertex_normal(v, polyhedron);
	      monge_form.comply_wrt_given_normal(n);

	      double k1 = monge_form.coefficients()[0];
	      double k2 = monge_form.coefficients()[1];


	      //= (2/pi)*(k2+k1/k2-k1)
	      double si_value = (2*(atan( (k2+k1)/(k2-k1) )))/pi;


	      //= sqrt((k1^2 + k2^2)/2)
	      double ci_value = sqrt( (pow(k1,2) + pow(k2,2))/2 );

#ifdef LOG
	      std::cout <<"k1  " << k1 << std::endl;
	      std::cout <<"k2  " << k2 << std::endl;
	      std::cout <<"SI  " << si_value << std::endl;
	      std::cout <<"CI  " << ci_value << std::endl;
#endif


		      if(si_value >= -1 && si_value <= 1) {
		    	  si_values_map[v] = si_value;
		      } else          {
		    	  si_values_map[v] = 0.0;
		      }

	    }
	  }

}


void Polyhedron_demo_mesh_segmentation_plugin::on_SDF_button_clicked()
{
    CGAL::Three::Scene_interface::Item_id index = scene->mainSelectionIndex();
    Scene_polyhedron_item* item = qobject_cast<Scene_polyhedron_item*>(scene->item(index));
    if(!item) { return; }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    std::size_t number_of_rays = ui_widget.Number_of_rays_spin_box->value();
    double cone_angle = (ui_widget.Cone_angle_spin_box->value()  / 180.0) * CGAL_PI;
    bool create_new_item = ui_widget.New_item_check_box->isChecked();
    
    Item_sdf_map::iterator pair;
    Scene_polyhedron_item* active_item = item;

    if(create_new_item) {
        active_item = new Scene_polyhedron_item(*item->polyhedron()); 
        active_item->setGouraudMode();
    }
    
    pair = item_sdf_map.insert(
            std::make_pair(active_item, std::vector<double>()) ).first; 
    
    check_and_set_ids(pair->first->polyhedron());
    pair->second.resize(item->polyhedron()->size_of_facets(), 0.0);
    Polyhedron_with_id_to_vector_property_map<Polyhedron, double>  sdf_pmap(&pair->second);
    QTime time;
    time.start();
    std::pair<double, double> min_max_sdf = sdf_values(*(pair->first->polyhedron()), sdf_pmap, cone_angle, number_of_rays);
    std::cout << "ok (" << time.elapsed() << " ms)" << std::endl;

    std::cout << "SDF computation is completed. Min-SDF : " << min_max_sdf.first << " " "Max-SDF : " << min_max_sdf.second << std::endl;

    pair->first->set_color_vector_read_only(true);
    colorize_sdf(pair->first, sdf_pmap, pair->first->color_vector());
       
    pair->first->setName(tr("(SDF-%1-%2)").arg(number_of_rays).arg(ui_widget.Cone_angle_spin_box->value()));
    
    if(create_new_item) {
        scene->addItem(pair->first);
        item->setVisible(false);
        scene->itemChanged(item);
        scene->itemChanged(pair->first);
        scene->setSelectedItem(index);
    }
    else {
      item->invalidate_buffers();
      scene->itemChanged(index);
    }

    QApplication::restoreOverrideCursor();
}



std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeGlobalMetric(int indexA, int indexB)
{
    std::vector<double> gMetric;
    double global_metric = 0;
    double improved_global_metric = 0;
    Scene_polyhedron_item* itemA = qobject_cast<Scene_polyhedron_item*>(scene->item(indexA));
    Scene_polyhedron_item* itemB = qobject_cast<Scene_polyhedron_item*>(scene->item(indexB));

    //Eigen Values and centroid
    double centroidA[3];
    EigenAnalysis eigenanalysisA;
    ComputeEigenAnalysis(*itemA->polyhedron(),eigenanalysisA,centroidA);

    double centroidB[3];
    EigenAnalysis eigenanalysisB;
    ComputeEigenAnalysis(*itemB->polyhedron(),eigenanalysisB,centroidB);


    //Compute Eccentricity
    double eigen_value_ratioA = eigenanalysisA.l1/eigenanalysisA.l2;
    double eigen_value_ratioB = eigenanalysisB.l1/eigenanalysisB.l2;

    //Find difference
    double eigen_value_diff = fabs(eigen_value_ratioA - eigen_value_ratioB);

#ifdef LOG
    std::cout << "eigen_value_ratioA  " << eigen_value_ratioA << std::endl;
    std::cout << "eigen_value_ratioB  " << eigen_value_ratioB << std::endl;
    std::cout << "eigen_value_diff  " << eigen_value_diff << std::endl;
#endif


     std::vector <double> eigen_shapeA = ComputeEigenShape(eigenanalysisA);
     std::vector <double>  eigen_shapeB = ComputeEigenShape(eigenanalysisB);
     double eigen_shape_diff = ComputeEuclideanDistanceForEigenSig(eigen_shapeA, eigen_shapeB);

#ifdef LOG
     std::cout << "eigen_shapeA  " << eigen_shapeA[0]
               << " "<< eigen_shapeA[1] <<" "<< eigen_shapeA[2] << std::endl;
     std::cout << "eigen_shapeB  " << eigen_shapeB[0]
               << " "<< eigen_shapeB[1] <<" "<< eigen_shapeB[2] << std::endl;
     std::cout << "eigen_shape_diff  " << eigen_shape_diff << std::endl;
#endif

// Get SI signature of meshes
    std::vector<double> si_sigA = ComputeSISignature(*itemA->polyhedron());
    std::vector<double> si_sigB = ComputeSISignature(*itemB->polyhedron());

#ifdef LOG
    PrintSISignature(si_sigA,"meshA");
    PrintSISignature(si_sigB,"meshB");
#endif
    //Compute SDF sigs
    std::vector<double> sdf_sigA = ComputeSDFSignature(*itemA->polyhedron());
    std::vector<double> sdf_sigB = ComputeSDFSignature(*itemB->polyhedron());

#ifdef LOG
    PrintSDFSignature(sdf_sigA,"meshA");
    PrintSDFSignature(sdf_sigB,"meshB");
#endif


    //TODO
    // EMD for SI global signatures
    //Find EMD between global sdf sigs
    float si_sig_diff =  ComputeEMDForSegmentSignatures(si_sigA, si_sigB);
    float eu_si_sig_diff = ComputeEuclideanDistance(si_sigA, si_sigB);

    //Find EMD between global sdf sigs
    float sdf_sig_diff =  ComputeEMDForSegmentSignatures(sdf_sigA, sdf_sigB);

    //Find EMD between global sdf sigs
    float eu_sdf_sig_diff =  ComputeEuclideanDistance(sdf_sigA, sdf_sigB);

#ifdef LOG
    std::cout << "sdf_sig_diff  " << sdf_sig_diff << std::endl;
    std::cout << "eu_sdf_sig_diff  " << eu_sdf_sig_diff << std::endl;

    std::cout << "si_sig_diff  " << si_sig_diff << std::endl;
    std::cout << "eu_si_sig_diff  " << eu_si_sig_diff << std::endl;
#endif
    //Final global Metric
    global_metric = 100*sdf_sig_diff + 10*eigen_shape_diff;

    //improved_global_metric = sqrt((100*sdf_sig_diff)*(100*sdf_sig_diff) + (10*eigen_shape_diff)*(10*eigen_shape_diff));

    improved_global_metric = sqrt((10*sdf_sig_diff)*(10*sdf_sig_diff) + (10*si_sig_diff)*(10*si_sig_diff) + (10*eigen_shape_diff)*(10*eigen_shape_diff));

    gMetric.push_back(global_metric);
    gMetric.push_back(improved_global_metric);

    return gMetric;
}




double Polyhedron_demo_mesh_segmentation_plugin::GetSurfaceArea(int index)
{
    Scene_polyhedron_item* item = qobject_cast<Scene_polyhedron_item*>(scene->item(index));
   // if(!item) { return; }
    double area = CGAL::Polygon_mesh_processing::area(*item->polyhedron());
    return area;
}

void Polyhedron_demo_mesh_segmentation_plugin::PrintAdjMat(int adjmat[][MAX_SEGMENTS],int dimension)
{
    for(int i = 0; i < dimension; i++)
    {
        for(int j = 0; j < dimension; j++)
            std::cout << adjmat[i][j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Polyhedron_demo_mesh_segmentation_plugin::ClearGraph()
{
    areaMeshA = -1;
    areaMeshB = -1;
    myGraphA.clear();
    myGraphB.clear();
    PolySegmentsA.clear();
    PolySegmentsB.clear();
    propsA.clear();
    propsB.clear();

}


void Polyhedron_demo_mesh_segmentation_plugin::on_CompareModels_clicked()
{
    QTime time;
    time.start();


    int adjmatA[MAX_SEGMENTS][MAX_SEGMENTS] = {0};
    int adjmatB[MAX_SEGMENTS][MAX_SEGMENTS] = {0};



    ClearGraph();

    //Segment and Compute Signature of Mesh A
    on_Partition_button_clicked(scene->selectionAindex(),PolySegmentsA,adjmatA);
#ifdef LOG
    PrintAdjMat(adjmatA,PolySegmentsA.size());
#endif
    areaMeshA = GetSurfaceArea(scene->selectionAindex());


    //Segment and Compute Signature of Mesh B
    on_Partition_button_clicked(scene->selectionBindex(),PolySegmentsB,adjmatB);
#ifdef LOG
    PrintAdjMat(adjmatB,PolySegmentsB.size());
#endif
    areaMeshB = GetSurfaceArea(scene->selectionBindex());



    //Let's start simple now. The segments can be greater than the MAX_SEGMENTS,thats a loop hole
    myGraphA = CreateSegmentARG(adjmatA,PolySegmentsA.size());
    myGraphB = CreateSegmentARG(adjmatB,PolySegmentsB.size());


// Call this function if you wan to test Hungarian Algo
    //TestLibHungarian();

    //Compute  Signature of Segments of A

    for(int i = 0;i < PolySegmentsA.size() ; i++)
    {
        propsA.push_back(UnaryAttributes());
        //Set segname
        std::stringstream segname;
        segname << "PolysegA" << i ;
        propsA[i].segment_name = segname.str();

        propsA[i].polyseg = PolySegmentsA[i];

        propsA[i].vert_index = i;
        propsA[i].IsMatched = false;
        propsA[i].matchedToIndex = -1;


        //Eigen Values and centroid
        double centroid[3];
        EigenAnalysis eigenanalysis;
        ComputeEigenAnalysis(PolySegmentsA[i],eigenanalysis,centroid);
#ifdef LOG
        std::cout << segname.str() <<std::endl;
#endif
        //Set centroid of Polyseg
        propsA[i].segcentroid << centroid[0],centroid[1],centroid[2];

        //Set Eigenvectors of polyseg e1>e2>e3
        propsA[i].EigenVectors << eigenanalysis.e1[0],eigenanalysis.e1[1],eigenanalysis.e1[2],
                eigenanalysis.e2[0],eigenanalysis.e2[1],eigenanalysis.e2[2],
                eigenanalysis.e3[0],eigenanalysis.e3[1],eigenanalysis.e3[2];

        //Compute Eccentricity of segment
        propsA[i].eigen_value_ratio = eigenanalysis.l1/eigenanalysis.l2;


        //Global Shape from eigen values
        propsA[i].eigen_shape = ComputeEigenShape(eigenanalysis);


        // Set surface area ratio
        if (PolySegmentsA[i].is_pure_triangle())
        {
           // compute the surface area
           double area = CGAL::Polygon_mesh_processing::area(PolySegmentsA[i]);
           propsA[i].segment_area_ratio = area/areaMeshA;
        }

        //SI signatures for A
        propsA[i].si_signature = ComputeSISignature(PolySegmentsA[i]);

        //PrintSISignature(propA.si_signature,segname.str());


        //SDF signatures for A
        propsA[i].sdf_signature = ComputeSDFSignature(PolySegmentsA[i]);

        //Clear the seg name
        segname.str( std::string() );
        segname.clear();


    }

    //Compute Signature of Segments of B
    for(int i = 0;i < PolySegmentsB.size() ; i++)
    {
        propsB.push_back(UnaryAttributes());
        std::stringstream segname;
        segname << "PolysegB" << i ;
        propsB[i].segment_name = segname.str();

        propsB[i].polyseg = PolySegmentsB[i];


        propsB[i].vert_index = i;
        propsB[i].IsMatched = false;
        propsB[i].matchedToIndex = -1;

        //Eigen Values
        double centroid[3];
        EigenAnalysis eigenanalysis;
        ComputeEigenAnalysis(PolySegmentsB[i],eigenanalysis,centroid);
#ifdef LOG
        std::cout << segname.str() <<std::endl;
#endif
        //Set centroid of Polyseg
        propsB[i].segcentroid << centroid[0],centroid[1],centroid[2];

        //Compute Eccentricity of segment
        propsB[i].eigen_value_ratio = eigenanalysis.l1/eigenanalysis.l2;

        //Global Shape from eigen values
        propsB[i].eigen_shape = ComputeEigenShape(eigenanalysis);

        //Set Eigenvectors of polyseg e1>e2>e3
        propsB[i].EigenVectors << eigenanalysis.e1[0],eigenanalysis.e1[1],eigenanalysis.e1[2],
                eigenanalysis.e2[0],eigenanalysis.e2[1],eigenanalysis.e2[2],
                eigenanalysis.e3[0],eigenanalysis.e3[1],eigenanalysis.e3[2];

         // Set surface area ratio
        if (PolySegmentsB[i].is_pure_triangle())
        {
            // compute the surface area
            double area = CGAL::Polygon_mesh_processing::area(PolySegmentsB[i]);
            propsB[i].segment_area_ratio = area/areaMeshB;

        }


        //SI signatures for B
        propsB[i].si_signature = ComputeSISignature(PolySegmentsB[i]);
        //PrintSISignature(propB.si_signature,segname.str());

        //SDF signatures for B
        propsB[i].sdf_signature = ComputeSDFSignature(PolySegmentsB[i]);
        //PrintSDFSignature(propB.sdf_signature,segname.str());

        //Clear the seg name
        segname.str( std::string() );
        segname.clear();

    }


    PopulateUnaryProps(myGraphA, propsA);
    PopulateUnaryProps(myGraphB, propsB);

    PopulateBinaryProps(myGraphA);
    PopulateBinaryProps(myGraphB);

#ifdef LOG
    //Test Sanity of Graphs
   TestSegArg(myGraphA);
   TestSegArg(myGraphB);
#endif


#ifdef LOCAL_NORMALIZATION
    //Normalizes for a particular segment-signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute(myGraphA,myGraphB);
#endif

#ifdef GLOBAL_LINEAR_NORMALIZATION
//Linearly Normalizes across all the segments for a particular signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute2(myGraphA,myGraphB);
#endif

#ifdef GLOBAL_NON_LINEAR_NORMALIZATION
//Log Normalizes across all the segments for a particular signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute3(myGraphA,myGraphB);
#endif


   Eigen::MatrixXd PermMat(CostMat.rows(),CostMat.cols());


   int rawPermMat[MAX_SEGMENTS][MAX_SEGMENTS];
   ComputePermutationMatrixForARGS(CostMat,PermMat,rawPermMat);

   //New code
   UpdatePolysegAssignment(myGraphA,myGraphB,rawPermMat);

#ifdef LOG
   PrintMatching(myGraphA);
   PrintMatching(myGraphB);
#endif



   int segment_diff = abs(PolySegmentsB.size() - PolySegmentsA.size());
   int max_number_seg = (PolySegmentsA.size() > PolySegmentsB.size()) ? PolySegmentsA.size() : PolySegmentsB.size();

   double part_based_metric = ComputePartBasedMetric(CostMat,PermMat,segment_diff);
   double improved_part_based_metric = ComputeImprovedPartBasedMetric(CostMat,PermMat,segment_diff);


   //Compute Global Feature based Metric, multiply with the max no. of segments to add weightage
   std::vector<double> global_metric = ComputeGlobalMetric(scene->selectionAindex(),scene->selectionBindex());

#ifdef LOG
   std::cout << std::endl;
   std::cout << "part_based_metric :: " << part_based_metric << std::endl;
   std::cout << "improved_part_based_metric :: " << improved_part_based_metric << std::endl;

   std::cout << "global_metric :: " << max_number_seg*global_metric[0] << std::endl;
   std::cout << "Improved global_metric :: " << max_number_seg*global_metric[1] << std::endl;

#endif
   //Add to get total Metric
   double ComparisonMetric =  global_metric[0] + part_based_metric;

   double Improved_ComparisonMetric =  sqrt(max_number_seg*max_number_seg*global_metric[1]*global_metric[1] + improved_part_based_metric*improved_part_based_metric);

#ifdef LOG
   //Print on Console
   std::cout << "ComparisonMetric :: " << ComparisonMetric;
   std::cout << std::endl;
   std::cout << "Improved_ComparisonMetric :: " << Improved_ComparisonMetric;
   std::cout << std::endl;

#endif


   std::cout << __FUNCTION__ << " " << "ok (" << time.elapsed() << " ms)" << std::endl;

}


void Polyhedron_demo_mesh_segmentation_plugin::on_RefineCompareModels_clicked()
{
    QTime time;
    time.start();

    boost::graph_traits<AttrRelGraph>::vertex_iterator viA, vi_endA;
    boost::graph_traits<AttrRelGraph>::vertex_iterator viB, vi_endB;
    std::string mergedPolySegname;
    std::vector< std::pair<int,int> > mergedSegIDs;


    if(boost::num_vertices(myGraphA) > boost::num_vertices(myGraphB))
    {
        //There will un matched Polysegs in graphA

        for (boost::tie(viA ,vi_endA) = boost::vertices(myGraphA); viA != vi_endA; ++viA)
        {
            std::cout << __FUNCTION__ << __LINE__ << std::endl;
        }


    }
    else
    {
        //There will un matched Polysegs in graphB
        for (boost::tie(viB ,vi_endB) = boost::vertices(myGraphB); viB != vi_endB; ++viB)
        {

            //So this is one of the unmatched Polyseg
            if(myGraphB[*viB].IsMatched == false)
            {

                std::map < double, std::pair< int,int > > MergeChoices;
                std::pair< boost::graph_traits<AttrRelGraph>::out_edge_iterator, boost::graph_traits<AttrRelGraph >::out_edge_iterator>
                        vi = boost::out_edges(*viB, myGraphB);

                boost::graph_traits<AttrRelGraph>::out_edge_iterator ei;

                for(ei = vi.first; ei != vi.second; ei++)
                {
                    //GetNeighBour(ei,myGraphB);
                   boost::graph_traits<AttrRelGraph>::vertex_descriptor nbr_vertex;
                  nbr_vertex = boost::target(*ei, myGraphB);
#ifdef LOG
                   std::cout <<"Neighbouring Vertex Name " << myGraphB[nbr_vertex].segment_name << std::endl;
                   std::cout <<"Neighbouring Vertex Index  " << myGraphB[nbr_vertex].vert_index << std::endl;
                   std::cout << "Merging " <<  myGraphB[*viB].segment_name << " AND ";
                   std::cout << myGraphB[nbr_vertex].segment_name << std::endl;
#endif

                   // write the polyhedron out as a .OFF file
                   std::stringstream filename;
                   filename << "MergedPolyseg_" <<  myGraphB[*viB].vert_index << "_" << myGraphB[nbr_vertex].vert_index <<".off";

                   mergedPolySegname = filename.str();


                   //Merge only with the matched neighbour
                   if(myGraphB[nbr_vertex].IsMatched == true)
                   {
                       Polyhedron MergedPoly;
                       MergeSegments(PolySegmentsB[(myGraphB[*viB].vert_index)],
                               PolySegmentsB[(myGraphB[nbr_vertex].vert_index)],
                               mergedPolySegname,
                               MergedPoly);


                       UnaryAttributes propsMerged;
                       //Since we are merging mesh B so sending its area as well.
                       ComputeUnaryProps(MergedPoly,propsMerged,mergedPolySegname,areaMeshB);


                       DistanceMeasures distOrig, distMerged;

                       //Find distance with the orignally matched segement of A
                       //propsA a global vector contains the properties of the segments
#ifdef LOG
                       std::cout << "matched to index  " << myGraphB[nbr_vertex].matchedToIndex << std::endl;
#endif
                       CompareDistances(propsA[(myGraphB[nbr_vertex].matchedToIndex)],propsB[(myGraphB[nbr_vertex].vert_index)],distOrig);

                       CompareDistances(propsA[(myGraphB[nbr_vertex].matchedToIndex)],propsMerged,distMerged);


                       bool decision_merge = GetDecisionLogical(distOrig,distMerged);


                       double collated_dist = distMerged.emd_sdf_sig + distMerged.eu_eigen_shape_descr
                               + distMerged.emd_si_sig + distMerged.segment_area_ratio_diff;

                       //create the pair of sgement ids which will be merged, and map to the distance with the template segment
                       MergeChoices[collated_dist] = std::make_pair((myGraphB[*viB].vert_index),(myGraphB[nbr_vertex].vert_index));
#ifdef LOG

                       std::cout << mergedPolySegname <<" decision_merge " << decision_merge << std::endl;
#endif

                   }


                }


#if 0
                //find min
                double min_dist = MergeChoices.begin()->second;
                for (std::map < std::string, double >::iterator it = MergeChoices.begin(); it != MergeChoices.end(); ++it)
                {
                    if(it->second < min_dist )
                        min_dist = it->second;
                }

                //get name for min value
                std::string MergeChoiceFinal;
                for (std::map < std::string, double >::iterator it = MergeChoices.begin(); it != MergeChoices.end(); ++it)
                {
                    if(it->second == min_dist )
                        MergeChoiceFinal = it->first;
                }

                std::cout << "decision_merge final " << MergeChoiceFinal << std::endl;

#endif

                // a map stores the data with key in sorted order--ascending, so take min value using begin
                std::pair<int,int > merge_seg_ids = MergeChoices.begin()->second;
#ifdef LOG
                std::cout << "decision_merge final " << merge_seg_ids.first <<" And " << merge_seg_ids.second << std::endl;
#endif
                mergedSegIDs.push_back(merge_seg_ids);


            }


        }

        //reconfigure the graph i.e. merge vertices.
        ReconfigureGraph(myGraphB,mergedSegIDs);

#ifdef LOG
        TestSegArg(myGraphB);
#endif

    }


#ifdef LOCAL_NORMALIZATION
    //Normalizes for a particular segment-signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute(myGraphA,myGraphB);
#endif

#ifdef GLOBAL_LINEAR_NORMALIZATION
//Linearly Normalizes across all the segments for a particular signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute2(myGraphA,myGraphB);
#endif

#ifdef GLOBAL_NON_LINEAR_NORMALIZATION
//Log Normalizes across all the segments for a particular signature
    Eigen::MatrixXf CostMat = ComputeDistanceForUnaryAttribute3(myGraphA,myGraphB);
#endif

    Eigen::MatrixXd PermMat(CostMat.rows(),CostMat.cols());


    int rawPermMat[MAX_SEGMENTS][MAX_SEGMENTS];
    ComputePermutationMatrixForARGS(CostMat,PermMat,rawPermMat);

    //New code
    UpdatePolysegAssignment(myGraphA,myGraphB,rawPermMat);
#ifdef LOG
    PrintMatching(myGraphA);
    PrintMatching(myGraphB);
#endif


    int segment_diff = abs(num_vertices(myGraphB) - num_vertices(myGraphA));
    int max_number_seg = (num_vertices(myGraphA) > num_vertices(myGraphB)) ? num_vertices(myGraphA) : num_vertices(myGraphB);

    double part_based_metric = ComputePartBasedMetric(CostMat,PermMat,segment_diff);
    double improved_part_based_metric = ComputeImprovedPartBasedMetric(CostMat,PermMat,segment_diff);


    //Compute Global Feature based Metric, multiply with the max no. of segments to add weightage
    std::vector<double> global_metric = ComputeGlobalMetric(scene->selectionAindex(),scene->selectionBindex());

#ifdef LOG

    std::cout << std::endl;
    std::cout << "part_based_metric :: " << part_based_metric << std::endl;
    std::cout << "improved_part_based_metric :: " << improved_part_based_metric << std::endl;

    std::cout << "global_metric :: " << max_number_seg*global_metric[0] << std::endl;
    std::cout << "Improved global_metric :: " << max_number_seg*global_metric[1] << std::endl;

#endif


    //Add to get total Metric
    double ComparisonMetric =  global_metric[0] + part_based_metric;

    double Improved_ComparisonMetric =  sqrt(max_number_seg*max_number_seg*global_metric[1]*global_metric[1] + improved_part_based_metric*improved_part_based_metric);

#ifdef LOG
    //Print on Console
    std::cout << "ComparisonMetric :: " << ComparisonMetric;
    std::cout << std::endl;
    std::cout << "Improved_ComparisonMetric :: " << Improved_ComparisonMetric;
    std::cout << std::endl;
#endif


 std::cout << __FUNCTION__ << " " << "ok (" << time.elapsed() << " ms)" << std::endl;

}


void Polyhedron_demo_mesh_segmentation_plugin::SavePolysegs(AttrRelGraph &myGraph)
{
    boost::graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;

    for (boost::tie(vi ,vi_end) = boost::vertices(myGraph); vi != vi_end; ++vi)
    {
        // write the polyhedron out as a .OFF file
        std::stringstream segname;
        segname << myGraph[*vi].segment_name <<".off";
        std::string name = segname.str();


        char *cstr = new char[name.length() + 1];
        strcpy(cstr, name.c_str());
        std::ofstream os(cstr);

        //output the file
        os << myGraph[*vi].polyseg;
        os.close();
        delete [] cstr;


        //Clear the seg
        segname.str( std::string() );
        segname.clear();
        name.clear();
    }


}

void Polyhedron_demo_mesh_segmentation_plugin::ReComputeProps(AttrRelGraph &myGraph)
{
    boost::graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;
    std::vector <UnaryAttributes> props;
    int i = 0;

    for (boost::tie(vi ,vi_end) = boost::vertices(myGraph); vi != vi_end; ++vi)
    {

        props.push_back(UnaryAttributes());
        //Set segname
        std::stringstream segname;
        segname << "PolysegM" << i ;
        props[i].segment_name = segname.str();


        //Just doing it again, to keep the things simple
        props[i].polyseg = myGraph[*vi].polyseg;

        props[i].vert_index = i;
        props[i].IsMatched = false;
        props[i].matchedToIndex = -1;


        //Eigen Values and centroid
        double centroid[3];
        EigenAnalysis eigenanalysis;
        ComputeEigenAnalysis(myGraph[*vi].polyseg,eigenanalysis,centroid);
#ifdef LOG
        std::cout << segname.str() <<std::endl;
#endif

        //Set centroid of Polyseg
        props[i].segcentroid << centroid[0],centroid[1],centroid[2];

        //Set Eigenvectors of polyseg e1>e2>e3
        props[i].EigenVectors << eigenanalysis.e1[0],eigenanalysis.e1[1],eigenanalysis.e1[2],
                eigenanalysis.e2[0],eigenanalysis.e2[1],eigenanalysis.e2[2],
                eigenanalysis.e3[0],eigenanalysis.e3[1],eigenanalysis.e3[2];

        //Compute Eccentricity of segment
        props[i].eigen_value_ratio = eigenanalysis.l1/eigenanalysis.l2;


        //Global Shape from eigen values
        props[i].eigen_shape = ComputeEigenShape(eigenanalysis);


        // Set surface area ratio
        if (myGraph[*vi].polyseg.is_pure_triangle())
        {
           // compute the surface area
           double area = CGAL::Polygon_mesh_processing::area(myGraph[*vi].polyseg);
           props[i].segment_area_ratio = area/areaMeshA;
        }

        //SI signatures for A
        props[i].si_signature = ComputeSISignature(myGraph[*vi].polyseg);

        //PrintSISignature(propA.si_signature,segname.str());


        //SDF signatures for A
        props[i].sdf_signature = ComputeSDFSignature(myGraph[*vi].polyseg);

        //Clear the seg name
        segname.str( std::string() );
        segname.clear();

        i++;

    }

    PopulateUnaryProps(myGraph, props);
    PopulateBinaryProps(myGraph);

}


 bool Polyhedron_demo_mesh_segmentation_plugin::GetDecisionLogical(DistanceMeasures distOrig,DistanceMeasures distMerged)
 {
     bool decision_sdf = distOrig.emd_sdf_sig > distMerged.emd_sdf_sig ? true : false;
     bool decision_si = distOrig.emd_si_sig > distMerged.emd_si_sig ? true : false;
     bool decision_eigen = distOrig.eu_eigen_shape_descr > distMerged.eu_eigen_shape_descr ? true : false;
     bool decision_area = distOrig.segment_area_ratio_diff > distMerged.segment_area_ratio_diff ? true : false;

#ifdef LOG
     std::cout <<"decision_sdf " << decision_sdf << std::endl;
     std::cout <<"decision_si " << decision_si << std::endl;
     std::cout <<"decision_eigen " << decision_eigen << std::endl;
     std::cout <<"decision_area " << decision_area << std::endl;
#endif

     if((decision_sdf + decision_si + decision_eigen + decision_area) >= 2)
         return true;
     else
         return false;

 }



 void Polyhedron_demo_mesh_segmentation_plugin::contract_vertices(boost::graph_traits<AttrRelGraph>::vertex_descriptor b,
                        boost::graph_traits<AttrRelGraph>::vertex_descriptor a,
                        AttrRelGraph& myGraph)
 {
     boost::graph_traits<AttrRelGraph>::adjacency_iterator vi, vi_end;
     for (boost::tie(vi, vi_end) = boost::adjacent_vertices(b, myGraph); vi != vi_end; ++vi)
     {
         if(a != *vi)//it should not be the same vertex wth which m merging,avoid cycles
         boost::add_edge(a, *vi, myGraph);
     }

     std::stringstream name;
     Polyhedron MergedPoly;
     name << myGraph[b].vert_index << "_" << myGraph[a].vert_index;

     //merge the polysegs
     MergeSegments(myGraph[b].polyseg,
             myGraph[a].polyseg,
             name.str(),
             MergedPoly);

     //clear the initial polyseg
     myGraph[a].polyseg.clear();

     //assign merged poly
     myGraph[a].polyseg = MergedPoly;

     //remove all the edges to this vertex
     boost::clear_vertex(b, myGraph);

     //remove the vertex as well
     boost::remove_vertex(b, myGraph);
 }



 bool Polyhedron_demo_mesh_segmentation_plugin::ReconfigureGraph(AttrRelGraph &myGraph,std::vector< std::pair<int,int> > &mergedSegIDs )
 {
     for(std::vector<std::pair<int,int> >::iterator it = mergedSegIDs.begin(); it != mergedSegIDs.end(); ++it)
     {
         boost::graph_traits<AttrRelGraph>::vertex_descriptor v1;
         boost::graph_traits<AttrRelGraph>::vertex_descriptor v2;

         boost::graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;

         for (boost::tie(vi ,vi_end) = boost::vertices(myGraph); vi != vi_end; ++vi)
         {
             if(myGraph[*vi].vert_index == it->first)
                 v1 = *vi;
             if(myGraph[*vi].vert_index == it->second)
                 v2 = *vi;

         }

         contract_vertices(v1,v2,myGraph);


     }



     //Compute the props again
     ReComputeProps(myGraph);

#ifdef LOG
     //Save the polysegs
     SavePolysegs(myGraph);
#endif


 }



void Polyhedron_demo_mesh_segmentation_plugin::ComputeUnaryProps(Polyhedron &PolyMesh, UnaryAttributes &props,
                                                                 std::string polysegname,double fullMeshArea)
{
    //Set segname
    props.segment_name = polysegname;

    props.vert_index = -1;
    props.IsMatched = false;


    //Eigen Values and centroid
    double centroid[3];
    EigenAnalysis eigenanalysis;
    ComputeEigenAnalysis(PolyMesh,eigenanalysis,centroid);

    //Set centroid of Polyseg
    props.segcentroid << centroid[0],centroid[1],centroid[2];

    //Set Eigenvectors of polyseg e1>e2>e3
    props.EigenVectors << eigenanalysis.e1[0],eigenanalysis.e1[1],eigenanalysis.e1[2],
            eigenanalysis.e2[0],eigenanalysis.e2[1],eigenanalysis.e2[2],
            eigenanalysis.e3[0],eigenanalysis.e3[1],eigenanalysis.e3[2];

    //Compute Eccentricity of segment
    props.eigen_value_ratio = eigenanalysis.l1/eigenanalysis.l2;


    //Global Shape from eigen values
    props.eigen_shape = ComputeEigenShape(eigenanalysis);


    // Set surface area ratio
    if (PolyMesh.is_pure_triangle())
    {
       // compute the surface area
       double area = CGAL::Polygon_mesh_processing::area(PolyMesh);
       props.segment_area_ratio = area/fullMeshArea;
    }
    props.si_signature = ComputeSISignature(PolyMesh);
    props.sdf_signature = ComputeSDFSignature(PolyMesh);

}


void Polyhedron_demo_mesh_segmentation_plugin::on_Partition_button_clicked
            (int index,std::vector <Polyhedron> &PolySegments,int adjmat[][MAX_SEGMENTS])
{    
    Scene_polyhedron_item* item = qobject_cast<Scene_polyhedron_item*>(scene->item(index));
    if(!item) { return; }
    
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    std::size_t number_of_clusters = ui_widget.Number_of_clusters_spin_box->value();
    double smoothness = ui_widget.Smoothness_spin_box->value();
    std::size_t number_of_rays = ui_widget.Number_of_rays_spin_box->value();
    double cone_angle = (ui_widget.Cone_angle_spin_box->value()  / 180.0) * CGAL_PI;
    bool create_new_item = ui_widget.New_item_check_box->isChecked();
    bool extract_segments = ui_widget.Extract_segments_check_box->isChecked();

    Item_sdf_map::iterator pair;
    if(create_new_item)
    {
        // create new item
        Scene_polyhedron_item* new_item = new Scene_polyhedron_item(*item->polyhedron()); 
        new_item->setGouraudMode(); 
        
        // copy SDF values of existing poly to new poly
        Item_sdf_map::iterator it = item_sdf_map.find(item);
        const std::vector<double>& sdf_data = it == item_sdf_map.end() ?
                                              std::vector<double>() : it->second;
        pair = item_sdf_map.insert(std::make_pair(new_item, sdf_data) ).first;
    }
    else
    {
        std::pair<Item_sdf_map::iterator, bool> res = 
          item_sdf_map.insert(std::make_pair(item, std::vector<double>()) );
        pair = res.first;
    }

    check_and_set_ids(pair->first->polyhedron());
    QTime time;
    time.start();
    if(pair->second.empty()) { // SDF values are empty, calculate
      pair->second.resize(pair->first->polyhedron()->size_of_facets(), 0.0);
      Polyhedron_with_id_to_vector_property_map<Polyhedron, double> sdf_pmap(&pair->second);
      sdf_values(*(pair->first->polyhedron()), sdf_pmap, cone_angle, number_of_rays); 
    }

    std::vector<std::size_t> internal_segment_map(pair->first->polyhedron()->size_of_facets());
    Polyhedron_with_id_to_vector_property_map<Polyhedron, std::size_t> segment_pmap(&internal_segment_map);
    Polyhedron_with_id_to_vector_property_map<Polyhedron, double> sdf_pmap(&pair->second);

    std::size_t nb_segments = segmentation_from_sdf_values(*(pair->first->polyhedron())
        ,sdf_pmap, segment_pmap, number_of_clusters, smoothness, extract_segments); 
    std::cout << "ok (" << time.elapsed() << " ms)" << std::endl;
#ifdef LOG
    std::cout << "Segmentation is completed. Number of segments : " << nb_segments << std::endl;  
#endif
    pair->first->set_color_vector_read_only(true);  
     
    colorize_segmentation(pair->first, segment_pmap, pair->first->color_vector());
    pair->first->setName(tr("(Segmentation-%1-%2)").arg(number_of_clusters).arg(smoothness));   


    PolySegments.resize(nb_segments);
    //Gets the segments as seperate meshes
    GetSegments(pair->first, segment_pmap,nb_segments,PolySegments,index);


    //Create the adjacence matrix from the segments
    CreateAdjMat(pair->first, segment_pmap,adjmat);


    if(create_new_item) {
        scene->addItem(pair->first);
        item->setVisible(false);
        scene->itemChanged(item);
        scene->itemChanged(pair->first);
        scene->setSelectedItem(index);
    }
    else {
      item->invalidate_buffers();
      scene->itemChanged(index);
    }

    QApplication::restoreOverrideCursor();
}

void Polyhedron_demo_mesh_segmentation_plugin::check_and_set_ids(Polyhedron* polyhedron)
{
    Polyhedron::Facet_iterator a_facet = polyhedron->facets_begin();
    Polyhedron::Facet_iterator another_facet = ++polyhedron->facets_begin();
    if(a_facet->id() != another_facet->id()) { return; } // ids are OK
    std::size_t facet_id = 0;
    for(Polyhedron::Facet_iterator facet_it = polyhedron->facets_begin();
        facet_it != polyhedron->facets_end(); ++facet_it, ++facet_id)
    {
        facet_it->id() = facet_id;
    }

}
void Polyhedron_demo_mesh_segmentation_plugin::check_and_set_vertex_ids(Polyhedron* polyhedron)
{
    Polyhedron::Vertex_iterator a_vertex = polyhedron->vertices_begin();
    Polyhedron::Vertex_iterator another_vertex = ++polyhedron->vertices_begin();
    if(a_vertex->id() != another_vertex->id()) { return; } // ids are OK
    std::size_t vertex_id = 0;
    for(Polyhedron::Vertex_iterator vertex_it = polyhedron->vertices_begin();
        vertex_it != polyhedron->vertices_end(); ++vertex_it, ++vertex_id)
    {
        vertex_it->id() = vertex_id;
    }

}
template<class SDFPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::colorize_sdf(
     Scene_polyhedron_item* item,
     SDFPropertyMap sdf_values,  
     std::vector<QColor>& color_vector)
{
    item->setItemIsMulticolor(true);
    Polyhedron* polyhedron = item->polyhedron();
    color_vector.clear();
    std::size_t patch_id = 0;
    for(Polyhedron::Facet_iterator facet_it = polyhedron->facets_begin(); 
        facet_it != polyhedron->facets_end(); ++facet_it, ++patch_id)   
    {
        double sdf_value = sdf_values[facet_it]; 
        int gray_color = static_cast<int>(255 * sdf_value);
        if(gray_color < 0 || gray_color >= 256) {
          color_vector.push_back(QColor::fromRgb(0,0,0));
        }
        else {
          color_vector.push_back(color_map_sdf[gray_color]);
        }
        facet_it->set_patch_id(static_cast<int>(patch_id));
    }
}

template<class SegmentPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::colorize_segmentation(
     Scene_polyhedron_item* item,
     SegmentPropertyMap segment_ids,
     std::vector<QColor>& color_vector)
{

    item->setItemIsMulticolor(true);
    Polyhedron* polyhedron = item->polyhedron();
    color_vector.clear();
    std::size_t max_segment = 0;
    for(Polyhedron::Facet_iterator facet_it = polyhedron->facets_begin(); 
        facet_it != polyhedron->facets_end(); ++facet_it)   
    {
        std::size_t segment_id = segment_ids[facet_it];
        facet_it->set_patch_id(static_cast<int>(segment_id));
        max_segment = (std::max)(max_segment, segment_id);
    }
#ifdef LOG
    std::cout << "max_segment: " << max_segment << std::endl;
#endif
    for(std::size_t i = 0; i <= max_segment; ++i)   
    {
        QColor aColor = color_map_segmentation[(max_segment - i) % color_map_segmentation.size()]; 
        color_vector.push_back(aColor);     
    }

}



template<class SegmentPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::CreateAdjMat(Scene_polyhedron_item* item,
     SegmentPropertyMap segment_ids, int adjmat[][MAX_SEGMENTS])
{
    Polyhedron* polyhedron = item->polyhedron();
    int current_segment = -1;
    int prev_segment = -1;
    for(Polyhedron::Edge_iterator edge_it = polyhedron->edges_begin();
        edge_it != polyhedron->edges_end(); ++edge_it)
    {
         current_segment = segment_ids[ (*edge_it).facet()];
         prev_segment = segment_ids[(*(edge_it->opposite())).facet()];
        if(current_segment != prev_segment )
            adjmat[current_segment][prev_segment] = 1;

    }

}





//#define DEBUG_SEGMENTS

typedef CGAL::Simple_cartesian<double>     K2;
typedef CGAL::Polyhedron_3<K2>         Polyhedron2;
typedef Polyhedron::HalfedgeDS             HalfedgeDS2;


template<class SegmentPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::GetSegments(
     Scene_polyhedron_item* item,
     SegmentPropertyMap segment_ids,int nb_segments,std::vector <Polyhedron> &PolySegments, int index)
{
	typedef typename Polyhedron::HalfedgeDS::Vertex   Vertex;
	typedef typename Vertex::Point Point;

	Polyhedron* polyhedron = item->polyhedron();
    //std::vector <Polyhedron> PolySegments(nb_segments);
    std::vector <int> segment_facet_count(nb_segments, 0);

    int polyseg = 0;

    int seg_id = -1;
    int face_count = 0;
    for(Polyhedron::Facet_iterator facet_it = polyhedron->facets_begin();
            facet_it != polyhedron->facets_end(); ++facet_it)
    {
        seg_id = segment_ids[facet_it];
        segment_facet_count[seg_id] = segment_facet_count[seg_id] + 1;

    }


    /*Notes:
     *   Observed that the no. of faces for the segments is coming to be same.
     *  For instance for the legs of Dino. But I think for a non symmetric model
     *  It would be different I think for different meshes
     *
     */

    for (std::vector<int>::iterator it = segment_facet_count.begin() ; it != segment_facet_count.end(); ++it)
    {
#ifdef LOG
        std::cout << ' ' << *it;
#endif
        face_count = face_count + *it;
    }
#ifdef LOG
    std::cout << std::endl <<"face_count " << face_count << std::endl;
#endif

    while(polyseg < nb_segments)
    {

         //Lets populate the vector for a segment.
         std::vector<double> coords;
         int i = 0;
         std::vector<int> tris;
         for(Polyhedron::Facet_iterator facet_it = polyhedron->facets_begin();
                 facet_it != polyhedron->facets_end(); ++facet_it)
         {



             if(segment_ids[facet_it] == polyseg)
             {

                 //Let's take the tree vertices of facet
                 Point a = facet_it->halfedge()->vertex()->point();
                 Point b = facet_it->halfedge()->next()->vertex()->point();
                 Point c = facet_it->halfedge()->opposite()->vertex()->point() ;

                 //Push first vertex of facet
                 coords.push_back(a.x());
                 coords.push_back(a.y());
                 coords.push_back(a.z());


                 //Push Second vertex of facet
                 coords.push_back(b.x());
                 coords.push_back(b.y());
                 coords.push_back(b.z());

                 //Push Third vertex of facet
                 coords.push_back(c.x());
                 coords.push_back(c.y());
                 coords.push_back(c.z());

                    facet_it->halfedge()->vertex()->id() = i;
                    tris.push_back(facet_it->halfedge()->vertex()->id());

                    i++;


                    facet_it->halfedge()->next()->vertex()->id() = i;
                    tris.push_back(facet_it->halfedge()->next()->vertex()->id());

                    i++;



                    facet_it->halfedge()->opposite()->vertex()->id() = i;
                    tris.push_back(facet_it->halfedge()->opposite()->vertex()->id());
                    i++;


#ifdef DEBUG_SEGMENTS
                 std::cout << facet_it->halfedge()->vertex()->id();
                 std::cout << " ";
                 std::cout << facet_it->halfedge()->next()->vertex()->id();
                 std::cout << " ";
                 std::cout << facet_it->halfedge()->opposite()->vertex()->id();
                 std::cout << std::endl;
#endif
             }


         }

#ifdef DEBUG_SEGMENTS
         std::cout << std::endl << " #Starts here" << std::endl;
         for (std::vector<double>::iterator it = coords.begin(); it != coords.end(); )
         {
             std::cout << "v" << " " ;
             std::cout << *it << " " ;
             ++it;
             std::cout << *it << " " ;
             ++it;
             std::cout << *it << " " ;
             ++it;
             std::cout << std::endl;
         }

         for (std::vector<int>::iterator it = tris.begin(); it != tris.end(); )
         {
             std::cout << "f" << " " ;
             std::cout << *it << " " ;
             ++it;
             std::cout << *it<< " " ;
             ++it;
             std::cout << *it << " " ;
             ++it;
             std::cout << std::endl;
         }
         std::cout << std::endl << " #Ends here" << std::endl;
#endif
         BuildSegments(coords,tris,PolySegments[polyseg]);

         // write the polyhedron out as a .OFF file
         std::stringstream filename;
         filename << "polyseg_" << index << "_" << polyseg <<".off";

         std::string name = filename.str();
         char *cstr = new char[name.length() + 1];
         strcpy(cstr, name.c_str());

         std::ofstream os(cstr);
         os << PolySegments[polyseg];
         os.close();
         polyseg++;
         filename.clear();
         delete [] cstr;
    }


    //std::cout << " PolySegments.size() : " << PolySegments.size() << std::endl;

}




void Polyhedron_demo_mesh_segmentation_plugin::MergeSegments(Polyhedron &PolySeg1,Polyhedron &PolySeg2,
                                                             std::string name, Polyhedron &MergedPoly)
{
    typedef typename Polyhedron::HalfedgeDS::Vertex   Vertex;
    typedef typename Vertex::Point Point;


    //Lets populate the vector for a segment.
    std::vector<double> coords;
    int i = 0;
    std::vector<int> tris;

    for(Polyhedron::Facet_iterator facet_it = PolySeg1.facets_begin();
            facet_it != PolySeg1.facets_end(); ++facet_it)
    {

        //Let's take the tree vertices of facet
        Point a = facet_it->halfedge()->vertex()->point();
        Point b = facet_it->halfedge()->next()->vertex()->point();
        Point c = facet_it->halfedge()->opposite()->vertex()->point() ;

        //Push first vertex of facet
        coords.push_back(a.x());
        coords.push_back(a.y());
        coords.push_back(a.z());


        //Push Second vertex of facet
        coords.push_back(b.x());
        coords.push_back(b.y());
        coords.push_back(b.z());

        //Push Third vertex of facet
        coords.push_back(c.x());
        coords.push_back(c.y());
        coords.push_back(c.z());

        facet_it->halfedge()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->vertex()->id());

        i++;


        facet_it->halfedge()->next()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->next()->vertex()->id());

        i++;



        facet_it->halfedge()->opposite()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->opposite()->vertex()->id());
        i++;



    }



    for(Polyhedron::Facet_iterator facet_it = PolySeg2.facets_begin();
            facet_it != PolySeg2.facets_end(); ++facet_it)
    {

        //Let's take the tree vertices of facet
        Point a = facet_it->halfedge()->vertex()->point();
        Point b = facet_it->halfedge()->next()->vertex()->point();
        Point c = facet_it->halfedge()->opposite()->vertex()->point() ;

        //Push first vertex of facet
        coords.push_back(a.x());
        coords.push_back(a.y());
        coords.push_back(a.z());


        //Push Second vertex of facet
        coords.push_back(b.x());
        coords.push_back(b.y());
        coords.push_back(b.z());

        //Push Third vertex of facet
        coords.push_back(c.x());
        coords.push_back(c.y());
        coords.push_back(c.z());

        facet_it->halfedge()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->vertex()->id());

        i++;


        facet_it->halfedge()->next()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->next()->vertex()->id());

        i++;



        facet_it->halfedge()->opposite()->vertex()->id() = i;
        tris.push_back(facet_it->halfedge()->opposite()->vertex()->id());
        i++;



    }



    BuildSegments(coords,tris,MergedPoly);


    char *cstr = new char[name.length() + 1];
    strcpy(cstr, name.c_str());
    std::ofstream os(cstr);
    os << MergedPoly;
    os.close();
    delete [] cstr;

}




void Polyhedron_demo_mesh_segmentation_plugin::ComputeEigenAnalysis(Polyhedron &polyhedron,
                                                                    EigenAnalysis &eigenanalysis,double *centroid )
{
    typedef CGAL::Exact_predicates_inexact_constructions_kernel K1;
    typedef CGAL::Mesh_3::Robust_intersection_traits_3<kernel_type_h::K1> Kernel;
    typedef Kernel::Triangle_3 Triangle;
    typedef Kernel::Point_3 Point;
    typedef Kernel::Line_3 Line;

    // get triangles from the mesh
    std::list<Triangle> triangles;
    Polyhedron::Facet_iterator f;
    for(f = polyhedron.facets_begin();
      f != polyhedron.facets_end();
      ++f)
    {
      const Point& a = f->halfedge()->vertex()->point();
      const Point& b = f->halfedge()->next()->vertex()->point();
      const Point& c = f->halfedge()->prev()->vertex()->point();
      triangles.push_back(Triangle(a,b,c));
    }

    // compute centroid
     Point center_of_mass = CGAL::centroid(triangles.begin(),triangles.end());
     centroid[0] = center_of_mass.x();
     centroid[1] = center_of_mass.y();
     centroid[2] = center_of_mass.z();

    // fit line to triangles
    Line line;
    std::cout << "Fit line...";
    CGAL::linear_least_squares_fitting_3(eigenanalysis,triangles.begin(),triangles.end(),line,CGAL::Dimension_tag<2>());
    std::cout << "ok" << std::endl;

}


std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeEigenShape(EigenAnalysis eigenanalysis)
{
    std::vector<double> eigen_shape;

    double sigma_li = eigenanalysis.l1 + eigenanalysis.l2 + eigenanalysis.l3 ;

    double alpha = (eigenanalysis.l1 - eigenanalysis.l2)/sigma_li ;
    double beta = 2*(eigenanalysis.l2 - eigenanalysis.l3)/sigma_li ;
    double gamma = 3*(eigenanalysis.l3)/sigma_li ;


    std::cout << alpha << " " << beta << " " << gamma;

    eigen_shape.push_back(alpha);
    eigen_shape.push_back(beta);
    eigen_shape.push_back(gamma);


    return eigen_shape;

}



/*
Question: How to use SI and CI both in a Signature
As of now creating a signature of SI value only, may be I will check
Should I create 2 Seperate signatures or a single 2D signature


*/
std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeSISignature(Polyhedron &polyhedron)
{
      typedef CGAL::Monge_via_jet_fitting<Kernel> Fitting;
      typedef Fitting::Monge_form Monge_form;

      typedef Kernel::Point_3 Point;
      std::vector< double > sivalues;
      std::vector< double > civalues;

      Polyhedron::Vertex_iterator v;
      for(v = polyhedron.vertices_begin();
          v != polyhedron.vertices_end();
          v++)
      {
            std::vector<Point> points;

            // pick central point
            const Point& central_point = v->point();
            points.push_back(central_point);

            Polyhedron::Halfedge_around_vertex_circulator he = v->vertex_begin();
            Polyhedron::Halfedge_around_vertex_circulator end = he;


            CGAL_For_all(he,end)
            {
                const Point& p = he->opposite()->vertex()->point();
                points.push_back(p);
            }


            ///TODO: Check the correct monge fit, use poly rings instead to compute the required no. of points
            /// Check the file Jet_fitting_3/examples/Jet_fitting_3/Mesh_estimation.cpp
            //std::cout << " points.size() : " << points.size() << std::endl;
            /*the fan size is of 3 triangles only, is it okay??
             *In the examples I saw to fit Monge when fan size is greater thatn 5
             * Need to check if it is okay or not
             * for the built segment it is coming 3, I guess it could be an issue ?
            */
            if(points.size() > 2)
            {
                // estimate curvature by fitting
                Fitting monge_fit;
                const int dim_monge = 2;
                const int dim_fitting = 2;
                Monge_form monge_form = monge_fit(points.begin(),points.end(),dim_fitting,dim_monge);

                // make monge form comply with vertex normal (to get correct
                // orientation)
                typedef Kernel::Vector_3 Vector;
                Vector n = CGAL::Polygon_mesh_processing::compute_vertex_normal(v, polyhedron);
                monge_form.comply_wrt_given_normal(n);

                double k1 = monge_form.coefficients()[0];
                double k2 = monge_form.coefficients()[1];


                //= (2/pi)*atan(k2+k1/k2-k1) -- originally in the paper [-1,1]
                // = 1/2 + (1/pi)atan(k2+k1/k2-k1 -- making it positive [0,1]
                double si_value = 1/2 + (atan( (k2+k1)/(k2-k1) ))/pi;
                sivalues.push_back(si_value);


                //= sqrt((k1^2 + k2^2)/2)
                double ci_value = sqrt( (pow(k1,2) + pow(k2,2))/2 );
                civalues.push_back(ci_value);

                /*
                std::cout <<"k1  " << k1 << std::endl;
                std::cout <<"k2  " << k2 << std::endl;
                std::cout <<"SI  " << si_value << std::endl;
                std::cout <<"CI  " << ci_value << std::endl;
                */
            }
      }

      //create an accumulator
        acc myAccumulator( boost::accumulators::tag::density::num_bins = 64,
                           boost::accumulators::tag::density::cache_size = sivalues.size());

       for(int i = 0;i < sivalues.size() ; i ++)
       {
           myAccumulator(sivalues[i]);
       }


       histogram_type hist = boost::accumulators::density(myAccumulator);

       std::vector<double> histvec;

       for(int i = 0;i < hist.size() ; i++)
           histvec.push_back(hist[i].second);

       return histvec;

}

std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeCISignature(Polyhedron &polyhedron)
{
      typedef CGAL::Monge_via_jet_fitting<Kernel> Fitting;
      typedef Fitting::Monge_form Monge_form;

      typedef Kernel::Point_3 Point;
      std::vector< double > civalues;

      Polyhedron::Vertex_iterator v;
      for(v = polyhedron.vertices_begin();
          v != polyhedron.vertices_end();
          v++)
      {
            std::vector<Point> points;

            // pick central point
            const Point& central_point = v->point();
            points.push_back(central_point);

            Polyhedron::Halfedge_around_vertex_circulator he = v->vertex_begin();
            Polyhedron::Halfedge_around_vertex_circulator end = he;


            CGAL_For_all(he,end)
            {
                const Point& p = he->opposite()->vertex()->point();
                points.push_back(p);
            }


            ///TODO: Check the correct monge fit, use poly rings instead to compute the required no. of points
            /// Check the file Jet_fitting_3/examples/Jet_fitting_3/Mesh_estimation.cpp
            //std::cout << " points.size() : " << points.size() << std::endl;
            /*the fan size is of 3 triangles only, is it okay??
             *In the examples I saw to fit Monge when fan size is greater thatn 5
             * Need to check if it is okay or not
             * for the built segment it is coming 3, I guess it could be an issue ?
            */
            if(points.size() > 2)
            {
                // estimate curvature by fitting
                Fitting monge_fit;
                const int dim_monge = 2;
                const int dim_fitting = 2;
                Monge_form monge_form = monge_fit(points.begin(),points.end(),dim_fitting,dim_monge);

                // make monge form comply with vertex normal (to get correct
                // orientation)
                typedef Kernel::Vector_3 Vector;
                Vector n = CGAL::Polygon_mesh_processing::compute_vertex_normal(v, polyhedron);
                monge_form.comply_wrt_given_normal(n);

                double k1 = monge_form.coefficients()[0];
                double k2 = monge_form.coefficients()[1];

                //= sqrt((k1^2 + k2^2)/2)
                double ci_value = sqrt( (pow(k1,2) + pow(k2,2))/2 );
                civalues.push_back(ci_value);

                /*
                std::cout <<"k1  " << k1 << std::endl;
                std::cout <<"k2  " << k2 << std::endl;
                std::cout <<"SI  " << si_value << std::endl;
                std::cout <<"CI  " << ci_value << std::endl;
                */
            }
      }

      //create an accumulator
        acc myAccumulator( boost::accumulators::tag::density::num_bins = 64,
                           boost::accumulators::tag::density::cache_size = civalues.size());

       for(int i = 0;i < civalues.size() ; i ++)
       {
           myAccumulator(civalues[i]);
       }


       histogram_type hist = boost::accumulators::density(myAccumulator);

       std::vector<double> histvec;

       for(int i = 0;i < hist.size() ; i++)
           histvec.push_back(hist[i].second);

       return histvec;

}

//TODO: Should I normalize the curvature values?
std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeGaussianCurvatureSignature(Polyhedron &polyhedron)
{
      typedef CGAL::Monge_via_jet_fitting<Kernel> Fitting;
      typedef Fitting::Monge_form Monge_form;

      typedef Kernel::Point_3 Point;
      std::vector< double > gaussian_curvature_values;

      Polyhedron::Vertex_iterator v;
      for(v = polyhedron.vertices_begin();
          v != polyhedron.vertices_end();
          v++)
      {
            std::vector<Point> points;

            // pick central point
            const Point& central_point = v->point();
            points.push_back(central_point);

            Polyhedron::Halfedge_around_vertex_circulator he = v->vertex_begin();
            Polyhedron::Halfedge_around_vertex_circulator end = he;


            CGAL_For_all(he,end)
            {
                const Point& p = he->opposite()->vertex()->point();
                points.push_back(p);
            }


            ///TODO: Check the correct monge fit, use poly rings instead to compute the required no. of points
            /// Check the file Jet_fitting_3/examples/Jet_fitting_3/Mesh_estimation.cpp
            //std::cout << " points.size() : " << points.size() << std::endl;
            /*the fan size is of 3 triangles only, is it okay??
             *In the examples I saw to fit Monge when fan size is greater thatn 5
             * Need to check if it is okay or not
             * for the built segment it is coming 3, I guess it could be an issue ?
            */
            if(points.size() > 2)
            {
                // estimate curvature by fitting
                Fitting monge_fit;
                const int dim_monge = 2;
                const int dim_fitting = 2;
                Monge_form monge_form = monge_fit(points.begin(),points.end(),dim_fitting,dim_monge);

                // make monge form comply with vertex normal (to get correct
                // orientation)
                typedef Kernel::Vector_3 Vector;
                Vector n = CGAL::Polygon_mesh_processing::compute_vertex_normal(v, polyhedron);
                monge_form.comply_wrt_given_normal(n);

                double k1 = monge_form.coefficients()[0];
                double k2 = monge_form.coefficients()[1];

                double value = k1*k2;
                gaussian_curvature_values.push_back(value);

            }
      }

      //create an accumulator
        acc myAccumulator( boost::accumulators::tag::density::num_bins = 64,
                           boost::accumulators::tag::density::cache_size = gaussian_curvature_values.size());

       for(int i = 0;i < gaussian_curvature_values.size() ; i ++)
       {
           myAccumulator(gaussian_curvature_values[i]);
       }


       histogram_type hist = boost::accumulators::density(myAccumulator);

       std::vector<double> histvec;

       for(int i = 0;i < hist.size() ; i++)
           histvec.push_back(hist[i].second);

       return histvec;

}


std::vector<double> Polyhedron_demo_mesh_segmentation_plugin::ComputeMeanCurvatureSignature(Polyhedron &polyhedron)
{
      typedef CGAL::Monge_via_jet_fitting<Kernel> Fitting;
      typedef Fitting::Monge_form Monge_form;

      typedef Kernel::Point_3 Point;
      std::vector< double > mean_curvature_values;

      Polyhedron::Vertex_iterator v;
      for(v = polyhedron.vertices_begin();
          v != polyhedron.vertices_end();
          v++)
      {
            std::vector<Point> points;

            // pick central point
            const Point& central_point = v->point();
            points.push_back(central_point);

            Polyhedron::Halfedge_around_vertex_circulator he = v->vertex_begin();
            Polyhedron::Halfedge_around_vertex_circulator end = he;


            CGAL_For_all(he,end)
            {
                const Point& p = he->opposite()->vertex()->point();
                points.push_back(p);
            }


            ///TODO: Check the correct monge fit, use poly rings instead to compute the required no. of points
            /// Check the file Jet_fitting_3/examples/Jet_fitting_3/Mesh_estimation.cpp
            //std::cout << " points.size() : " << points.size() << std::endl;
            /*the fan size is of 3 triangles only, is it okay??
             *In the examples I saw to fit Monge when fan size is greater thatn 5
             * Need to check if it is okay or not
             * for the built segment it is coming 3, I guess it could be an issue ?
            */
            if(points.size() > 2)
            {
                // estimate curvature by fitting
                Fitting monge_fit;
                const int dim_monge = 2;
                const int dim_fitting = 2;
                Monge_form monge_form = monge_fit(points.begin(),points.end(),dim_fitting,dim_monge);

                // make monge form comply with vertex normal (to get correct
                // orientation)
                typedef Kernel::Vector_3 Vector;
                Vector n = CGAL::Polygon_mesh_processing::compute_vertex_normal(v, polyhedron);
                monge_form.comply_wrt_given_normal(n);

                double k1 = monge_form.coefficients()[0];
                double k2 = monge_form.coefficients()[1];

                double value = (k1+k2)/2;
                mean_curvature_values.push_back(value);

            }
      }

      //create an accumulator
        acc myAccumulator( boost::accumulators::tag::density::num_bins = 64,
                           boost::accumulators::tag::density::cache_size = mean_curvature_values.size());

       for(int i = 0;i < mean_curvature_values.size() ; i ++)
       {
           myAccumulator(mean_curvature_values[i]);
       }


       histogram_type hist = boost::accumulators::density(myAccumulator);

       std::vector<double> histvec;

       for(int i = 0;i < hist.size() ; i++)
           histvec.push_back(hist[i].second);

       return histvec;

}






// A modifier creating a triangle with the incremental builder.
template<class HDS>
class polyhedron_builder : public CGAL::Modifier_base<HDS> {
public:
 std::vector<double> &coords;
 std::vector<int>    &tris;
    polyhedron_builder( std::vector<double> &_coords, std::vector<int> &_tris ) : coords(_coords), tris(_tris) {}
    void operator()( HDS& hds) {
  typedef typename HDS::Vertex   Vertex;
        typedef typename Vertex::Point Point;

  // create a cgal incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B( hds, true);
        B.begin_surface( coords.size()/3, tris.size()/3 );

  // add the polyhedron vertices
  for( int i=0; i<(int)coords.size(); i+=3 ){
   B.add_vertex( Point( coords[i+0], coords[i+1], coords[i+2] ) );
  }

  // add the polyhedron triangles
  for( int i=0; i<(int)tris.size(); i+=3 ){
   B.begin_facet();
   B.add_vertex_to_facet( tris[i+0] );
   B.add_vertex_to_facet( tris[i+1] );
   B.add_vertex_to_facet( tris[i+2] );
   B.end_facet();
  }

  // finish up the surface
        B.end_surface();
    }
};



int Polyhedron_demo_mesh_segmentation_plugin::BuildSegments(std::vector<double> &coords,std::vector<int> &tris, Polyhedron &P)
{
     polyhedron_builder<HalfedgeDS2 > builder( coords, tris );
     P.delegate( builder );
     return 0;
}


//Green:Spherical Cup
//Cyan:Trough
//Blue:Rut
//Pale Blue: Saddle rut
//White:Saddle
//Pale Yellow:Saddle ridge
//Yellow:Ridge
//Orange:Dome
//Red:Spherical cap

template<class SIPropertyMap>
void Polyhedron_demo_mesh_segmentation_plugin::colorize_si(
     Scene_polyhedron_item* item,
     SIPropertyMap si_values,  
     std::vector<QColor>& color_vector)
{

    item->setItemIsMulticolor(true);
    Polyhedron* polyhedron = item->polyhedron();
    color_vector.clear();

    std::size_t patch_id = 0;
    for(Polyhedron::Vertex_iterator vertex_it = polyhedron->vertices_begin(); 
        vertex_it != polyhedron->vertices_end(); ++vertex_it, ++patch_id)   
    {
        double si_value = si_values[vertex_it];

        std::cout << si_value << std::endl;
        if(-1 <= si_value && si_value < -0.875 )
        {
           color_vector.push_back(color_map_SI[0]);
           std::cout <<"Color Green:Spherical Cup"<< std::endl;
        }
        if(-0.875 <= si_value && si_value < -0.625 )
        {
        	color_vector.push_back(color_map_SI[1]);
        	std::cout <<"Color Cyan:Trough"<< std::endl;
        }
        if(-0.625 <= si_value && si_value < -0.375 )
        {
        	color_vector.push_back(color_map_SI[2]);
        	std::cout <<"Color Blue:Rut"<< std::endl;
        }
        if(-0.375 <= si_value && si_value < -0.25 )
        {
        	color_vector.push_back(color_map_SI[3]);
        	std::cout <<"Color Pale Blue: Saddle rut"<< std::endl;
        }
        if(-0.25 <= si_value && si_value < 0.25 )
        {
        	color_vector.push_back(color_map_SI[4]);
        	std::cout <<"Color White:Saddle"<< std::endl;
        }
        if(0.25 <= si_value && si_value < 0.375 )
        {
        	color_vector.push_back(color_map_SI[5]);
        	std::cout <<"Color  Pale Yellow:Saddle ridge"<< std::endl;
        }
        if(0.375 <= si_value && si_value < 0.625 )
        {
        	color_vector.push_back(color_map_SI[6]);
        	std::cout <<"Color Yellow:Ridge"<< std::endl;
        }
        if(0.625 <= si_value && si_value < 0.875 )
        {
        	color_vector.push_back(color_map_SI[7]);
        	std::cout <<"Color Orange:Dome"<< std::endl;
        }
        if(0.875 <= si_value && si_value < 1 )
        {
        	color_vector.push_back(color_map_SI[8]);
        	std::cout <<"Color Red:Spherical cap"<< std::endl;
        }

       // (*vertex_it).Facet_handle.set_patch_id(static_cast<int>(patch_id));

        //vertex_it->point();
        //vertex_it->facet();
        //vertex_it->halfedge()->facet()->set_patch_id(static_cast<int>(patch_id));
        //vertex_it->vertex();

        Polyhedron::Vertex myvert = *vertex_it;



        Polyhedron::Halfedge_around_vertex_circulator  hedgeb = myvert.vertex_begin(),hedgee = hedgeb;
        //Halfedge_around_vertex_circulator  hedgeb = v->vertex_begin(), hedgee = hedgeb;
         CGAL_For_all(hedgeb, hedgee){
        	 hedgeb->facet()->set_patch_id(static_cast<int>(patch_id));
         }
        ///vertex_it->Faset_patch_id(static_cast<int>(patch_id));
    }
}



/*

    Batch Processing comparison of signatures, based on Precision and Recall comparison

*/


//Paths of directories
#define SOURCE_PATH_STRING "/home/sukhraj/Projects/Acads/MSProject/PrecisionRecall/SourceData"
#define TARGET_PATH_STRING "/home/sukhraj/Projects/Acads/MSProject/PrecisionRecall/TargetData"
#define RESULT_PATH_STRING "/home/sukhraj/Projects/Acads/MSProject/PrecisionRecall/ResultDir"


void Polyhedron_demo_mesh_segmentation_plugin::OnBatchCompare()
{
    std::vector <std::string> src_filenames, trg_filenames;
    std::vector <ResultInfo> results;

    src_filenames = TraveseDir(SOURCE_PATH_STRING);
    trg_filenames = TraveseDir(TARGET_PATH_STRING);


    for ( std::vector<std::string>::iterator iter = src_filenames.begin(); iter != src_filenames.end(); iter++ ) {

        ResultInfo result;
        Polyhedron P_Src;
        std::stringstream src_file_path;
        src_file_path << SOURCE_PATH_STRING << "/" << *iter;
        ReadPolyhedron(src_file_path.str().c_str(),P_Src);
        result.polyname_src = *iter;

        std::vector<double> sdf_sigA = ComputeSDFSignature(P_Src);
        std::vector<double> si_sigA = ComputeSISignature(P_Src);

        std::vector<double> ci_sigA = ComputeCISignature(P_Src);

        std::vector<double> gaussian_sigA = ComputeGaussianCurvatureSignature(P_Src);
        std::vector<double> mean_sigA = ComputeMeanCurvatureSignature(P_Src);


        double centroidA[3];
        EigenAnalysis eigenanalysisA;
        ComputeEigenAnalysis(P_Src,eigenanalysisA,centroidA);
        std::vector <double> eigen_shapeA = ComputeEigenShape(eigenanalysisA);


        for ( std::vector<std::string>::iterator iter = trg_filenames.begin(); iter != trg_filenames.end(); iter++ ) {

            Polyhedron P_Trg;
            std::stringstream trg_file_path;
            trg_file_path << TARGET_PATH_STRING << "/" << *iter;
            ReadPolyhedron(trg_file_path.str().c_str(),P_Trg);
            result.polyname_trg = *iter;

            //For SDF Signature

            std::vector<double> sdf_sigB = ComputeSDFSignature(P_Trg);
            //Find EMD between global sdf sigs
            result.sdf_sig_diff =  ComputeEMDForSegmentSignatures(sdf_sigA, sdf_sigB);
            // For SI signature of meshes
            std::vector<double> si_sigB = ComputeSISignature(P_Trg);
            //Find EMD between global sdf sigs
            result.si_sig_diff =  ComputeEMDForSegmentSignatures(si_sigA, si_sigB);

            // For SI signature of meshes
            std::vector<double> ci_sigB = ComputeCISignature(P_Trg);
            //Find EMD between global sdf sigs
            result.ci_sig_diff =  ComputeEMDForSegmentSignatures(ci_sigA, ci_sigB);



            std::vector<double> gaussian_sigB = ComputeGaussianCurvatureSignature(P_Trg);
            result.gaussianc_sig_diff =  ComputeEMDForSegmentSignatures(gaussian_sigA, gaussian_sigB);

            std::vector<double> mean_sigB = ComputeMeanCurvatureSignature(P_Trg);
            result.meanc_sig_diff =  ComputeEMDForSegmentSignatures(mean_sigA, mean_sigB);





            //For eigen shape descriptor

            //Eigen Values and centroid


            double centroidB[3];
            EigenAnalysis eigenanalysisB;
            ComputeEigenAnalysis(P_Trg,eigenanalysisB,centroidB);
            std::vector <double>  eigen_shapeB = ComputeEigenShape(eigenanalysisB);
            result.eigen_shape_sig_diff = ComputeEuclideanDistanceForEigenSig(eigen_shapeA, eigen_shapeB);

            result.improved_global_metric = sqrt((10*result.sdf_sig_diff)*(10*result.sdf_sig_diff) +
                                            (10*result.si_sig_diff)*(10*result.si_sig_diff) +
                                            (10*result.eigen_shape_sig_diff)*(10*result.eigen_shape_sig_diff));

            results.push_back(result);

            //Clear vectors
            sdf_sigB.clear();
            si_sigB.clear();
            ci_sigB.clear();
            gaussian_sigB.clear();
            mean_sigB.clear();
            eigen_shapeB.clear();

        }

        //Clear vectors
        sdf_sigA.clear();
        si_sigA.clear();
        ci_sigA.clear();
        gaussian_sigA.clear();
        mean_sigA.clear();
        eigen_shapeA.clear();

    }

    SaveResults(results);


}



std::vector <std::string> Polyhedron_demo_mesh_segmentation_plugin::TraveseDir(std::string dir_path)
{
    DIR *dirp;
    struct dirent *dp;
    std::vector <std::string> filenames;
    if ((dirp = opendir(dir_path.c_str())) == NULL) {
            perror ("Cannot open ");
            exit (1);
    }

    while ((dp = readdir(dirp)) != NULL)
    {
        std::string filename(dp->d_name);
        if( filename.find ("off") != std::string::npos ) {
            filenames.push_back(filename);
        }

    }

    (void)closedir(dirp);
    return filenames;
}


int Polyhedron_demo_mesh_segmentation_plugin::ReadPolyhedron(const char* path,Polyhedron& P)
{

    std::cout << "Reading " << path << std::endl;
    //Create object
    std::ifstream in(path);


    if (!in)
    {
      std::cerr << "Cannot open file " << std::endl;
      return 1;
    }

    //Handle non-oriented meshes
    std::vector<Kernel::Point_3> points;
    std::vector< std::vector<std::size_t> > polygons;
    if (!CGAL::read_OFF(in, points, polygons))
    {
      std::cerr << "Error parsing the OFF file " << std::endl;
      return 1;
    }
    CGAL::Polygon_mesh_processing::orient_polygon_soup(points, polygons);
    CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(points, polygons, P);
    if (CGAL::is_closed(P) && (!CGAL::Polygon_mesh_processing::is_outward_oriented(P)))
      CGAL::Polygon_mesh_processing::reverse_face_orientations(P);

    in.close();

//    // to detect isolated vertices
//    CGAL::File_scanner_OFF scanner( in, false);
//    std::size_t total_nb_of_vertices = scanner.size_of_vertices();
//    std::cout << "total_nb_of_vertices  " << total_nb_of_vertices << std::endl;
//    in.seekg(0);

//    if ( in.good() ) {
//        //read the off file and populate the Polyhedron
//        in >> P;

//        in.close();

//    }

}

void Polyhedron_demo_mesh_segmentation_plugin::SaveResults(std::vector <ResultInfo>& results)
{
    std::ofstream result_file;
    std::stringstream file_path;
    file_path << RESULT_PATH_STRING << "/" << "Results.csv";
    std::cout << file_path.str() << std::endl;
    result_file.open (file_path.str().c_str());

    if (result_file.good())
    {
          result_file << "Generated using Anatomeco V1.0 " << std::endl;
          result_file << "SOURCE,TARGET,SDF_SIG_DIFF,SI_SIG_DIFF,CI_SIG_DIFF,GAUSSIANC_SIG_DIFF,MEANC_SIG_DIFF,EIGEN_SHAPE_DIFF,IMPVD_GLOBAL_METRIC"<< std::endl;

          for ( std::vector<ResultInfo>::iterator result_info_iter = results.begin();
                result_info_iter != results.end(); result_info_iter++ ) {
              result_file << (*result_info_iter).polyname_src<< ",";
              result_file << (*result_info_iter).polyname_trg << ",";
              result_file << (*result_info_iter).sdf_sig_diff << ",";
              result_file << (*result_info_iter).si_sig_diff << ",";
              result_file << (*result_info_iter).ci_sig_diff << ",";
              result_file << (*result_info_iter).gaussianc_sig_diff << ",";
              result_file << (*result_info_iter).meanc_sig_diff << ",";
              result_file << (*result_info_iter).eigen_shape_sig_diff << ",";
              result_file << (*result_info_iter).improved_global_metric << std::endl;
          }
    }
     result_file.close();
}

#include "Mesh_segmentation_plugin.moc"
