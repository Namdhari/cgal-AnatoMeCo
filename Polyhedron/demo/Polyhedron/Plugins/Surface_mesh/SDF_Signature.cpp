#include "SDF_Signature.h"




using namespace boost;
using namespace boost::accumulators;


//typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
//typedef CGAL::Polyhedron_3<Kernel> Polyhedron;

std::map <double,unsigned int> sdf_histogram;
#ifdef COMPUTE_SDF_WEIGHTS
typedef Kernel::Point_3;
namespace PMP = CGAL::Polygon_mesh_processing;
//compute weighted sdf for a facet
template <class myPolyhedron, class SDFPropertyMap, class SignaturePropertyMap>
void compute_facet_signature(const myPolyhedron& pmesh, SDFPropertyMap sdf_property_map,SignaturePropertyMap& signature_property_map)
{

Point_3 p(0,0,0),q(0,0,0),r(0,0,0); 

  double mesh_area = PMP::area(pmesh);
  std::cout << "mesh area = " << mesh_area << std::endl;
//compute face area
  for(Polyhedron::Facet_const_iterator facet_it = pmesh.facets_begin();
      facet_it != pmesh.facets_end(); ++facet_it) {
      double face_area = Kernel::Compute_area_3()(
	  facet_it->halfedge()->vertex()->point(),
          facet_it->halfedge()->next()->vertex()->point(),
          facet_it->halfedge()->opposite()->vertex()->point() );

  std::cout << "face area = " << face_area << std::endl;
//compute face area
      signature_property_map[facet_it] = (face_area*sdf_property_map[facet_it])/mesh_area;
  }

}

#endif

std::vector<double> ComputeSDFSignature(Polyhedron &polyhedron)
{
  // create a property-map
  typedef std::map<Polyhedron::Facet_const_handle, double> Facet_double_map;
  Facet_double_map internal_map;
  boost::associative_property_map<Facet_double_map> sdf_property_map(internal_map);

  // compute SDF values
  //by default postprocessing is set true
  std::pair<double, double> min_max_sdf = CGAL::sdf_values(polyhedron, sdf_property_map);

#ifdef COMPUTE_SDF_WEIGHTS
  // compute SDF signature values
  Facet_double_map internal_map2;
  boost::associative_property_map<Facet_double_map> signature_property_map(internal_map2);
  compute_facet_signature(polyhedron, sdf_property_map,signature_property_map);
#endif


  // It is possible to compute the raw SDF values and post-process them using
  // the following lines:
  // const std::size_t number_of_rays = 25;  // cast 25 rays per facet
  // const double cone_angle = 2.0 / 3.0 * CGAL_PI; // set cone opening-angle
  // CGAL::sdf_values(polyhedron, sdf_property_map, cone_angle, number_of_rays, false);
  // std::pair<double, double> min_max_sdf =
  //  CGAL::sdf_values_postprocessing(polyhedron, sdf_property_map);


  // print minimum & maximum SDF values
//  std::cout << "minimum SDF: " << min_max_sdf.first
  //          << " maximum SDF: " << min_max_sdf.second << std::endl;


std::cout << "sdf prop ap size " << internal_map.size() << std::endl;

//create an accumulator
// The bin size has been taken as 64, the same hase been taken in
//http://research.microsoft.com/en-us/um/people/rgal/publications/papers/pose_oblivious.pdf
  acc myAccumulator( tag::density::num_bins = 64, tag::density::cache_size = internal_map.size());


  std::cout << "==============================================================================" << std::endl;
  //print SDF value
  for(Polyhedron::Facet_const_iterator facet_it = polyhedron.facets_begin();
      facet_it != polyhedron.facets_end(); ++facet_it) {
//      std::cout << sdf_property_map[facet_it] << std::endl;
      myAccumulator(sdf_property_map[facet_it]);
  }

  //std::cout << std::endl;
  //std::cout << "==============================================================================" << std::endl;

  histogram_type hist = density(myAccumulator);
  double total = 0.0;

/* 
std::cout << "Hist lower bounds " << std::endl; 
  for( int i = 0; i < hist.size(); i++ ) 
  {
    std::cout << hist[i].first << std::endl;
  }
*/

#ifdef DEBUG
std::cout << "Hist values " << std::endl;
 for( int i = 0; i < hist.size(); i++ ) 
  {
    std::cout << hist[i].second << std::endl; 
    total += hist[i].second;
  }
#endif
//std::cout << "Total: " << total << std::endl;

#ifdef COMPUTE_SDF_WEIGHTS
// print SDF signature values
  for(Polyhedron::Facet_const_iterator facet_it = polyhedron.facets_begin();
      facet_it != polyhedron.facets_end(); ++facet_it) {
      std::cout << signature_property_map[facet_it] << " ";
  }
#endif


  std::vector<double> histvec;

  for(int i = 0;i < hist.size() ; i++)
      histvec.push_back(hist[i].second);

  return histvec;

}
