#include "segmentgraph.h"
//#include "emd.h"


using namespace boost;
using namespace std;

//Function to create attributed relational graph of segments
AttrRelGraph CreateSegmentARG(int adjmat[][MAX_SEGMENTS],int vertex_count)
{
    int status = 0;

    typedef std::pair<int, int> Edge;

    //Vector of all the edges
    std::vector<Edge> Edges;

    for(int i = 0; i < MAX_SEGMENTS; i++)
    {
        //The matix is square and symmetric so iterate till diagonal only
        for(int j = 0; j <= i; j++)
        {
            if (adjmat[i][j] == 1)//vertices i,j are adjacent
            {
                //Create an edge which is pair of nodes actually
                Edge(i,j);
                //Push it to the edges vector
                Edges.push_back(Edge(i,j));
            }

        }

    }


    //Create Graph but wihout properties
    AttrRelGraph myARG(vertex_count);

    for (int i = 0 ; i < Edges.size(); i++)
    {
        boost::add_edge(Edges[i].first,Edges[i].second,myARG);
    }


    //Create Graph but wihout properties
  //  AttrRelGraph myARG(Edges.begin(), Edges.end(), Edges.size());



    //Init Unary Props
    std::stringstream segname;
    int vertexcount = 0;
    graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;
    for (boost::tie(vi, vi_end) = vertices(myARG); vi != vi_end; ++vi)
    {
        segname << "Polyseg" << vertexcount;
        myARG[*vi].segment_name = segname.str();
        myARG[*vi].vert_index = vertexcount;

        myARG[*vi].segment_area_ratio = 0;
        myARG[*vi].eigen_value_ratio = 0;
        myARG[*vi].IsMatched = false;
        myARG[*vi].matchedToIndex = -1;

        //myARG[*vi].si_signature = {0};
        //myARG[*vi].sdf_signature = {0};
        ++vertexcount;

        //Clear the seg
        segname.str( std::string() );
        segname.clear();

    }

    //Init Binary Props
    graph_traits<AttrRelGraph>::edge_iterator ei, ei_end;
    for (boost::tie(ei, ei_end) = edges(myARG); ei != ei_end; ++ei)
    {
        myARG[*ei].segment_euclidean_distance = 0;
        myARG[*ei].angle_between_max_eigenvector = 0;
        myARG[*ei].angle_between_second_max_eigenvector = 0;
    }
#ifdef LOG
    std::cout << num_vertices(myARG) << std::endl;
    std::cout << num_edges(myARG) << std::endl;
#endif

    return myARG;
}



void PopulateUnaryProps(AttrRelGraph &myARG,std::vector<UnaryAttributes> props)
{
    int index = 0;
    //Init Unary Props
    graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;
    for (boost::tie(vi, vi_end) = vertices(myARG); vi != vi_end; ++vi)
    {
        myARG[*vi].segment_name = props[index].segment_name;
        myARG[*vi].vert_index = props[index].vert_index;
        myARG[*vi].IsMatched = props[index].IsMatched;
        myARG[*vi].polyseg = props[index].polyseg;
        myARG[*vi].matchedToIndex = props[index].matchedToIndex;
        myARG[*vi].segment_area_ratio = props[index].segment_area_ratio;
        myARG[*vi].eigen_value_ratio = props[index].eigen_value_ratio;
        myARG[*vi].eigen_shape = props[index].eigen_shape;
        myARG[*vi].segcentroid = props[index].segcentroid;
        myARG[*vi].EigenVectors = props[index].EigenVectors;
#ifdef LOG
        std::cout << myARG[*vi].si_signature.size() << std::endl;
#endif
        //for(int i = 0; i < props[index].si_signature.size() ; i++)
        myARG[*vi].si_signature = props[index].si_signature;
#ifdef LOG
        std::cout << myARG[*vi].si_signature.size() << std::endl;
        std::cout << myARG[*vi].sdf_signature.size() << std::endl;
#endif
        //for(int i = 0; i < props[index].sdf_signature.size() ; i++)
        myARG[*vi].sdf_signature = props[index].sdf_signature;
#ifdef LOG
        std::cout << myARG[*vi].sdf_signature.size() << std::endl;


        PrintSISignature(props[index].si_signature,props[index].segment_name);
        PrintSDFSignature(props[index].sdf_signature,props[index].segment_name);
#endif
        index++;
    }


}


void PopulateBinaryProps(AttrRelGraph &myARG/*,BinaryAttributes props*/)
{
    //Init Binary Props
    graph_traits<AttrRelGraph>::edge_iterator ei, ei_end;
    graph_traits<AttrRelGraph>::vertex_descriptor edge_source,edge_target;

    for (boost::tie(ei, ei_end) = edges(myARG); ei != ei_end; ++ei)
    {

        //Get source and target segment
        edge_source = source(*ei,myARG);
        edge_target = target(*ei,myARG);

        //Get euclidean distance between segments
        Eigen::Vector3f distance_vector = myARG[edge_source].segcentroid - myARG[edge_target].segcentroid;
        myARG[*ei].segment_euclidean_distance = distance_vector.norm();

        Eigen::Vector3d e1_source,e2_source;
        Eigen::Vector3d e1_target,e2_target;


        //Eigen stores in column major, so need to transpose
        Eigen::Matrix3d s_EigenVectors = myARG[edge_source].EigenVectors.transpose();
        Eigen::Matrix3d t_EigenVectors = myARG[edge_target].EigenVectors.transpose();

        // Get Max and second max eigen vector for source
        e1_source << s_EigenVectors.row(0);
        e2_source << s_EigenVectors.row(1);

        // Get Max and second max eigen vector for target
        e1_target << t_EigenVectors.row(0);
        e2_target << t_EigenVectors.row(1);

        double cos_theta = e1_source.dot(e1_target)/(e1_source.norm()*e1_target.norm());
        double cos_theta2 = e2_source.dot(e2_target)/(e2_source.norm()*e2_target.norm());

        /*
        if(cos_theta > 0)
            myARG[*ei].angle_between_max_eigenvector = acos(cos_theta);
        else if(cos_theta == 0)
            myARG[*ei].angle_between_max_eigenvector = pi/2;
        else if(cos_theta < 0)
         */
#ifdef LOG
        std::cout <<"For  " <<  myARG[edge_source].segment_name << " and " << myARG[edge_target].segment_name << std::endl;
        std::cout <<"cos_theta " << "cos_theta2 " << cos_theta << cos_theta2 << std::endl;
#endif
        //Compute angles in radians between eigen vectors of two segments and assign it as edge attribute
        myARG[*ei].angle_between_max_eigenvector = acos(cos_theta);
        myARG[*ei].angle_between_second_max_eigenvector = acos(cos_theta2);

#ifdef LOG
        std::cout <<"ang1 " << (180/pi)*myARG[*ei].angle_between_max_eigenvector << std::endl;
        std::cout <<"ang2 "  << (180/pi)*myARG[*ei].angle_between_second_max_eigenvector << std::endl;
#endif

    }

}




vector< pair<double,double> > GetEigenSignature(graph_traits<AttrRelGraph>::vertex_descriptor curr_vert,const AttrRelGraph &myARG,int max_degree)
{
    //graph_traits<AttrRelGraph>::edge_iterator ei, ei_end;



    //graph_traits<AttrRelGraph>::out_edge_iterator eit, eend;

    /*
    boost::tie(eit, eend) = boost::out_edges(curr_vert, myARG);
    std::for_each(eit, eend,[&myARG](graph_traits<AttrRelGraph>::edge_descriptor it)
          { std::cout << target(it, myARG) << '\n'; });
          */


    int deg = out_degree(curr_vert, myARG);
    int degree_diff = abs(deg - max_degree);
#ifdef LOG
    std::cout << " GetEigenSignature for " << myARG[curr_vert].segment_name<< std::endl;
#endif
    //std::vector<double> esig(2*max_vertex_size,0.0);
    //vector< pair<double,double> > esig(max_degree,make_pair(0.0,0.0));
    vector< pair<double,double> > esig;

    std::pair< graph_traits<AttrRelGraph>::out_edge_iterator, graph_traits<AttrRelGraph >::out_edge_iterator>
            vi = out_edges(curr_vert, myARG);

    graph_traits<AttrRelGraph>::out_edge_iterator ei;

    for(ei = vi.first; ei != vi.second; ei++)
    {

#ifdef LOG
        std::cout << " Between source: " << myARG[source(*ei, myARG)].segment_name ;
        std::cout << " And target " << myARG[target(*ei, myARG)].segment_name << std::endl;
#endif

       // std::cout << myARG[*ei].angle_between_max_eigenvector << std::endl;
        //esig.push_back( myARG[*ei].angle_between_max_eigenvector);
       // std::cout << myARG[*ei].angle_between_second_max_eigenvector << std::endl;
        //esig.push_back(myARG[*ei].angle_between_second_max_eigenvector);

        esig.push_back(std::make_pair(myARG[*ei].angle_between_max_eigenvector,
                       myARG[*ei].angle_between_second_max_eigenvector));

    }

    //Push zeros, for degree diff
    for(int i = 0 ; i < degree_diff; i++)
    {
        esig.push_back(std::make_pair(0.0,0.0));

    }


    return esig;
}


//How to use std::next_permutation
//http://stackoverflow.com/questions/24150840/next-permutation-compare-function
struct compareFirstPairMember {
    bool operator()(const pair<double, double>& a, const pair<double, double>& b) const {
        return a.first < b.first;
    }
};


//Function to find distance between binary descriptors
double ComputeOptimumDistanceForBinaryAttribute(std::vector< std::pair<double,double> > Sig1,
                                                std::vector< std::pair<double,double> > Sig2)
{

    Eigen::VectorXd v(2*Sig1.size());
    sort(Sig1.begin(), Sig1.end(), compareFirstPairMember());


    //taking some sufficient large no. to start
    double e = 1000000.0;
    do {
        //Loop for each permutation of template vertex
        int j = 0;
        for(int i = 0 ; i < Sig1.size() ;i++ )
        {
            //std::cout << " ( "<< Sig1[i].first << " , ";
            //First max angle diff
            v[j] = Sig1[i].first - Sig2[i].first;
            j++;
           // std::cout << Sig1[i].second << " )";
            //2nd max angle diff
            v[j] = Sig1[i].second - Sig2[i].second;
            j++;
        }
      //  std::cout << std::endl;

        //Float comparison :(
        if(v.norm() < e)
            e = v.norm();

      //  std::cout << "v.norm() = " << v.norm() << std::endl;
      //  std::cout << "e = " << e << std::endl;

      } while ( std::next_permutation(Sig1.begin(),Sig1.end(),compareFirstPairMember()) );


    return e;

}


//We are using 6 kinds of signatures
#define NUM_SIGNATURE_USED 5


void printmat(std::vector < std::vector < double > > & Cost)
{

    for (unsigned int i = 0; i < Cost.size(); ++i)
    {
        for (unsigned int j = 0; j < Cost[i].size(); ++j)
        {

           // std::cout << std::fixed << std::showpoint;
            std::cout << std::setprecision(10);
            std::cout << setw(16) << left << Cost[i][j] << " ";

           // cout << Cost[i][j] << " ";
        }
        cout << std::endl;
    }


}


void printvec( std::vector < double > & Cost)
{

    for (unsigned int i = 0; i < Cost.size(); ++i)
    {
        //std::cout << std::fixed << std::showpoint;
        std::cout << std::setprecision(6);
        std::cout << setw(16) << left << Cost[i] << " ";

    }

          std::cout << std::endl;

}

#ifdef LOCAL_NORMALIZATION
//Function to find distance between the unary descriptors
Eigen::MatrixXf ComputeDistanceForUnaryAttribute(AttrRelGraph myARGA,AttrRelGraph myARGB)
{
#ifdef LOG
    std::cout << " segment_area_ratio_diff " << "|" << " eccentricity_diff " << "|" << " eu_eigen_shape_descr " << "|" << "emd_si_sig" << "|"<< "emd_sdf_sig";
    std::cout <<"|"<< "eu_si_sig" << "|" << "eu_sdf_sig" << std::endl;
#endif


    Eigen::MatrixXf CostMat(num_vertices(myARGA),num_vertices(myARGB));


        int i = 0;
        graph_traits<AttrRelGraph>::vertex_iterator viA, vi_endA;
        graph_traits<AttrRelGraph>::vertex_iterator viB, vi_endB;

        std::vector < std::vector < double > > Cost;
        for (boost::tie(viA ,vi_endA) = vertices(myARGA); viA != vi_endA; ++viA)
        {
            std::vector < double > signature_tuple;
#ifdef LOG
            std::cout <<  " ===================================== " << std::endl;
#endif
            int j = 0;
            for (boost::tie(viB ,vi_endB) = vertices(myARGB); viB != vi_endB; ++viB)
            {

                int out_degreeA = out_degree(*viA, myARGA);
                int out_degreeB = out_degree(*viB, myARGB);

                //This takes into consideration a notion of connectivity
                double  degree_differnce_penalty =  abs(out_degreeA - out_degreeB); //This wt. multiplied is arbitrary

                //1
              //  signature_tuple.push_back(degree_differnce_penalty);
#ifdef LOG
                std::cout << "degree_differnce_penalty" << degree_differnce_penalty << std::endl;
#endif
                int max_degree = max(out_degree(*viA, myARGA),out_degree(*viB, myARGB));

                 //double eu_eigen_sig = ComputeEuclideanDistanceForEigenSig(GetEigenSignature(*viA,myARGA,max_vertex_size),GetEigenSignature(*viB,myARGB,max_vertex_size));
                double eu_eigen_sig = ComputeOptimumDistanceForBinaryAttribute(GetEigenSignature(*viA,myARGA,max_degree),
                                                                               GetEigenSignature(*viB,myARGB,max_degree));
                //2
                signature_tuple.push_back(eu_eigen_sig);
#ifdef LOG
                std::cout << "eu_eigen_sig " << eu_eigen_sig << std::endl;
#endif
                //Absolute distance between area ratios
                double segment_area_ratio_diff = abs(myARGA[*viA].segment_area_ratio -  myARGB[*viB].segment_area_ratio);

                //3
                signature_tuple.push_back(segment_area_ratio_diff);

                //Absolute distance between eccentricitiy
                double eccentricity_diff = abs(myARGA[*viA].eigen_value_ratio -  myARGB[*viB].eigen_value_ratio);

                //EMD distance between SI Signatures
                float emd_si_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //4
                signature_tuple.push_back(emd_si_sig);

                //EMD for SDF Signature of Segments
                float emd_sdf_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);

                //5
                signature_tuple.push_back(emd_sdf_sig);

                //Euclidean distance between eigen shape descriptor
                double eu_eigen_shape_descr = ComputeEuclideanDistanceForEigenSig(myARGA[*viA].eigen_shape,myARGB[*viB].eigen_shape);

                //6
                signature_tuple.push_back(eu_eigen_shape_descr);

                //Euclidean distance between SI signature
                double eu_si_sig = ComputeEuclideanDistance(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //Euclidean distance between SDF signature
                double eu_sdf_sig = ComputeEuclideanDistance(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);
#ifdef LOG
                std::cout << segment_area_ratio_diff << "|" << eccentricity_diff << "|" << eu_eigen_shape_descr << "|" << emd_si_sig << "|"<< emd_sdf_sig;
                std::cout <<"|"<< eu_si_sig << "|" << eu_sdf_sig << std::endl;
#endif
                //CostMat(i,j) = 20*segment_area_ratio_diff + 10*eu_eigen_shape_descr + 100*emd_si_sig + 200*emd_sdf_sig + degree_differnce_penalty
                //                + eu_eigen_sig; //eu_eigen_sig has some issues due to iteration
                j++;

                Cost.push_back(signature_tuple);
                signature_tuple.clear();
            }




            i++;
        }
#ifdef LOG
        std::cout << "================== Got Matrix Printing it ============ " << std::endl;
        printmat(Cost);
#endif
        for(int i = 0; i < NUM_SIGNATURE_USED; i++)
        {
            for(int j = 0; j < num_vertices(myARGA); j++)
            {
                std::vector < double > normsig;

                //Get all the signature values for this segment
                for(int k = 0; k < num_vertices(myARGB); k++)
                {
                    //For a particular signature i ,get distance between Aj,Bk segment
                    double dist = Cost.at( j*num_vertices(myARGB) + k ).at(i);
                    normsig.push_back(dist);
                }

#ifdef LOG
                printvec(normsig);
#endif


                //Find max value
                 std::vector<double>::iterator result = std::max_element(normsig.begin(), normsig.end());;
                 double max_dist = *result;

                  //std::cout << "max " << *result << std::endl;
                 //Normalize the vector values
                 if(max_dist != 0)
                 {
                     for(int p = 0 ; p < normsig.size() ; p++)
                     {
                         double normalized_val = normsig.at(p)/max_dist;
                         normsig.at(p) = normalized_val;


                     }


                 }
#ifdef LOG
                 printvec(normsig);
#endif
                 //assign the normalized vector back
                 for(int k = 0; k < num_vertices(myARGB); k++)
                 {

                     //For a particular signature i ,get distance between Aj,Bk segment
                     Cost.at( j*num_vertices(myARGB) + k).at(i) = normsig.at(k);

                 }

                 normsig.clear();

            }

        }

#ifdef LOG
         std::cout << " ========== Normalized Matrix Printing it ================ " << std::endl;
         printmat(Cost);
#endif

        for(int t = 0; t < num_vertices(myARGA) ; t++)
        {

            for(int u = 0; u < num_vertices(myARGB) ; u++)
            {

                double cumulative_dist = 0;

                //Get all the signature values for this segment
                for(int n = 0; n < NUM_SIGNATURE_USED; n++)
                {
                    cumulative_dist =  cumulative_dist + 10*Cost.at( t*num_vertices(myARGB) + u).at(n);

                }

                CostMat(t,u) = cumulative_dist;

            }

        }


        return CostMat;
}
#endif
#ifdef GLOBAL_LINEAR_NORMALIZATION

//Function to find distance between the unary descriptors
Eigen::MatrixXf ComputeDistanceForUnaryAttribute2(AttrRelGraph myARGA,AttrRelGraph myARGB)
{
#ifdef LOG
    std::cout << " segment_area_ratio_diff " << "|" << " eccentricity_diff " << "|" << " eu_eigen_shape_descr " << "|" << "emd_si_sig" << "|"<< "emd_sdf_sig";
    std::cout <<"|"<< "eu_si_sig" << "|" << "eu_sdf_sig" << std::endl;
#endif


    Eigen::MatrixXf CostMat(num_vertices(myARGA),num_vertices(myARGB));


        int i = 0;
        graph_traits<AttrRelGraph>::vertex_iterator viA, vi_endA;
        graph_traits<AttrRelGraph>::vertex_iterator viB, vi_endB;

        std::vector < std::vector < double > > Cost;
        for (boost::tie(viA ,vi_endA) = vertices(myARGA); viA != vi_endA; ++viA)
        {
            std::vector < double > signature_tuple;
#ifdef LOG
            std::cout <<  " ===================================== " << std::endl;
#endif
            int j = 0;
            for (boost::tie(viB ,vi_endB) = vertices(myARGB); viB != vi_endB; ++viB)
            {

                int out_degreeA = out_degree(*viA, myARGA);
                int out_degreeB = out_degree(*viB, myARGB);

                //This takes into consideration a notion of connectivity
                double  degree_differnce_penalty =  abs(out_degreeA - out_degreeB); //This wt. multiplied is arbitrary

                //1
              //  signature_tuple.push_back(degree_differnce_penalty);
#ifdef LOG
                std::cout << "degree_differnce_penalty" << degree_differnce_penalty << std::endl;
#endif
                int max_degree = max(out_degree(*viA, myARGA),out_degree(*viB, myARGB));

                 //double eu_eigen_sig = ComputeEuclideanDistanceForEigenSig(GetEigenSignature(*viA,myARGA,max_vertex_size),GetEigenSignature(*viB,myARGB,max_vertex_size));
                double eu_eigen_sig = ComputeOptimumDistanceForBinaryAttribute(GetEigenSignature(*viA,myARGA,max_degree),
                                                                               GetEigenSignature(*viB,myARGB,max_degree));
                //2
                signature_tuple.push_back(eu_eigen_sig);
#ifdef LOG
                std::cout << "eu_eigen_sig " << eu_eigen_sig << std::endl;
#endif
                //Absolute distance between area ratios
                double segment_area_ratio_diff = abs(myARGA[*viA].segment_area_ratio -  myARGB[*viB].segment_area_ratio);

                //3
                signature_tuple.push_back(segment_area_ratio_diff);

                //Absolute distance between eccentricitiy
                double eccentricity_diff = abs(myARGA[*viA].eigen_value_ratio -  myARGB[*viB].eigen_value_ratio);

                //EMD distance between SI Signatures
                float emd_si_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //4
                signature_tuple.push_back(emd_si_sig);

                //EMD for SDF Signature of Segments
                float emd_sdf_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);

                //5
                signature_tuple.push_back(emd_sdf_sig);

                //Euclidean distance between eigen shape descriptor
                double eu_eigen_shape_descr = ComputeEuclideanDistanceForEigenSig(myARGA[*viA].eigen_shape,myARGB[*viB].eigen_shape);

                //6
                signature_tuple.push_back(eu_eigen_shape_descr);

                //Euclidean distance between SI signature
                double eu_si_sig = ComputeEuclideanDistance(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //Euclidean distance between SDF signature
                double eu_sdf_sig = ComputeEuclideanDistance(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);
#ifdef LOG
                std::cout << segment_area_ratio_diff << "|" << eccentricity_diff << "|" << eu_eigen_shape_descr << "|" << emd_si_sig << "|"<< emd_sdf_sig;
                std::cout <<"|"<< eu_si_sig << "|" << eu_sdf_sig << std::endl;
#endif
                //CostMat(i,j) = 20*segment_area_ratio_diff + 10*eu_eigen_shape_descr + 100*emd_si_sig + 200*emd_sdf_sig + degree_differnce_penalty
                //                + eu_eigen_sig; //eu_eigen_sig has some issues due to iteration
                j++;

                Cost.push_back(signature_tuple);
                signature_tuple.clear();
            }




            i++;
        }
#ifdef LOG
        std::cout << "================== Got Matrix Printing it ============ " << std::endl;
        printmat(Cost);
#endif
        for(int i = 0; i < NUM_SIGNATURE_USED; i++)
        {
            std::vector < double > normsig;
            for(int j = 0; j < num_vertices(myARGA); j++)
            {
                //Get all the signature values for this segment
                for(int k = 0; k < num_vertices(myARGB); k++)
                {
                    //For a particular signature i ,get distance between Aj,Bk segment
                    double dist = Cost.at( j*num_vertices(myARGB) + k ).at(i);
                    normsig.push_back(dist);
                }

#ifdef LOG
                printvec(normsig);
#endif
            }


            //Find max value
             std::vector<double>::iterator result = std::max_element(normsig.begin(), normsig.end());
             std::vector<double>::iterator result_min = std::min_element(normsig.begin(), normsig.end());
             double max_dist = *result;
             double min_dist = *result_min;
              //std::cout << "max " << *result << std::endl;

#ifdef BASIC_NORMALIZATION
             //Normalize the vector values [min,1]
             if((max_dist) != 0)
             {
                 for(int p = 0 ; p < normsig.size() ; p++)
                 {
                     double normalized_val = normsig.at(p)/max_dist;
                     normsig.at(p) = normalized_val;
                 }

             }
#else
             //Normalize the vector values [0,1]
             if((max_dist - min_dist) != 0)
             {
                 for(int p = 0 ; p < normsig.size() ; p++)
                 {
                     double normalized_val = (normsig.at(p) - min_dist)/(max_dist - min_dist);
                     normsig.at(p) = normalized_val;
                 }

             }

#endif

#ifdef LOG
                 printvec(normsig);
#endif

                 for(int j = 0; j < num_vertices(myARGA); j++)
                 {
                          //assign the normalized vector back
                          for(int k = 0; k < num_vertices(myARGB); k++)
                          {

                              //For a particular signature i ,get distance between Aj,Bk segment
                              Cost.at( j*num_vertices(myARGB) + k).at(i) = normsig.at(j*num_vertices(myARGB)+k);

                          }
                 }

                 normsig.clear();
        }



#ifdef LOG
         std::cout << " ========== Normalized Matrix Printing it ================ " << std::endl;
         printmat(Cost);
#endif

        for(int t = 0; t < num_vertices(myARGA) ; t++)
        {

            for(int u = 0; u < num_vertices(myARGB) ; u++)
            {

                double cumulative_dist = 0;

                //Get all the signature values for this segment
                for(int n = 0; n < NUM_SIGNATURE_USED; n++)
                {
                    cumulative_dist =  cumulative_dist + 10*Cost.at( t*num_vertices(myARGB) + u).at(n);

                }

                CostMat(t,u) = cumulative_dist;

            }

        }


        return CostMat;
}

#endif

#ifdef GLOBAL_NON_LINEAR_NORMALIZATION
//Function to find distance between the unary descriptors
Eigen::MatrixXf ComputeDistanceForUnaryAttribute3(AttrRelGraph myARGA,AttrRelGraph myARGB)
{
#ifdef LOG
    std::cout << " segment_area_ratio_diff " << "|" << " eccentricity_diff " << "|" << " eu_eigen_shape_descr " << "|" << "emd_si_sig" << "|"<< "emd_sdf_sig";
    std::cout <<"|"<< "eu_si_sig" << "|" << "eu_sdf_sig" << std::endl;
#endif


    Eigen::MatrixXf CostMat(num_vertices(myARGA),num_vertices(myARGB));


        int i = 0;
        graph_traits<AttrRelGraph>::vertex_iterator viA, vi_endA;
        graph_traits<AttrRelGraph>::vertex_iterator viB, vi_endB;

        std::vector < std::vector < double > > Cost;
        for (boost::tie(viA ,vi_endA) = vertices(myARGA); viA != vi_endA; ++viA)
        {
            std::vector < double > signature_tuple;
#ifdef LOG
            std::cout <<  " ===================================== " << std::endl;
#endif
            int j = 0;
            for (boost::tie(viB ,vi_endB) = vertices(myARGB); viB != vi_endB; ++viB)
            {

                int out_degreeA = out_degree(*viA, myARGA);
                int out_degreeB = out_degree(*viB, myARGB);

                //This takes into consideration a notion of connectivity
                double  degree_differnce_penalty =  abs(out_degreeA - out_degreeB); //This wt. multiplied is arbitrary

                //1
              //  signature_tuple.push_back(degree_differnce_penalty);
#ifdef LOG
                std::cout << "degree_differnce_penalty" << degree_differnce_penalty << std::endl;
#endif
                int max_degree = max(out_degree(*viA, myARGA),out_degree(*viB, myARGB));

                 //double eu_eigen_sig = ComputeEuclideanDistanceForEigenSig(GetEigenSignature(*viA,myARGA,max_vertex_size),GetEigenSignature(*viB,myARGB,max_vertex_size));
                double eu_eigen_sig = ComputeOptimumDistanceForBinaryAttribute(GetEigenSignature(*viA,myARGA,max_degree),
                                                                               GetEigenSignature(*viB,myARGB,max_degree));
                //2
                signature_tuple.push_back(eu_eigen_sig);
#ifdef LOG
                std::cout << "eu_eigen_sig " << eu_eigen_sig << std::endl;
#endif
                //Absolute distance between area ratios
                double segment_area_ratio_diff = abs(myARGA[*viA].segment_area_ratio -  myARGB[*viB].segment_area_ratio);

                //3
                signature_tuple.push_back(segment_area_ratio_diff);

                //Absolute distance between eccentricitiy
                double eccentricity_diff = abs(myARGA[*viA].eigen_value_ratio -  myARGB[*viB].eigen_value_ratio);

                //EMD distance between SI Signatures
                float emd_si_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //4
                signature_tuple.push_back(emd_si_sig);

                //EMD for SDF Signature of Segments
                float emd_sdf_sig = ComputeEMDForSegmentSignatures(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);

                //5
                signature_tuple.push_back(emd_sdf_sig);

                //Euclidean distance between eigen shape descriptor
                double eu_eigen_shape_descr = ComputeEuclideanDistanceForEigenSig(myARGA[*viA].eigen_shape,myARGB[*viB].eigen_shape);

                //6
                signature_tuple.push_back(eu_eigen_shape_descr);

                //Euclidean distance between SI signature
                double eu_si_sig = ComputeEuclideanDistance(myARGA[*viA].si_signature,myARGB[*viB].si_signature);

                //Euclidean distance between SDF signature
                double eu_sdf_sig = ComputeEuclideanDistance(myARGA[*viA].sdf_signature,myARGB[*viB].sdf_signature);
#ifdef LOG
                std::cout << segment_area_ratio_diff << "|" << eccentricity_diff << "|" << eu_eigen_shape_descr << "|" << emd_si_sig << "|"<< emd_sdf_sig;
                std::cout <<"|"<< eu_si_sig << "|" << eu_sdf_sig << std::endl;
#endif
                //CostMat(i,j) = 20*segment_area_ratio_diff + 10*eu_eigen_shape_descr + 100*emd_si_sig + 200*emd_sdf_sig + degree_differnce_penalty
                //                + eu_eigen_sig; //eu_eigen_sig has some issues due to iteration
                j++;

                Cost.push_back(signature_tuple);
                signature_tuple.clear();
            }




            i++;
        }
#ifdef LOG
        std::cout << "================== Got Matrix Printing it ============ " << std::endl;
        printmat(Cost);
#endif
        for(int i = 0; i < NUM_SIGNATURE_USED; i++)
        {
            std::vector < double > lognormsig;
            for(int j = 0; j < num_vertices(myARGA); j++)
            {
                //Get all the signature values for this segment
                for(int k = 0; k < num_vertices(myARGB); k++)
                {
                    //For a particular signature i ,get distance between Aj,Bk segment
                    double dist = Cost.at( j*num_vertices(myARGB) + k ).at(i);
                    lognormsig.push_back(dist);

                }

#ifdef LOG
                printvec(lognormsig);
#endif
            }


            //Find max value
             std::vector<double>::iterator result = std::max_element(lognormsig.begin(), lognormsig.end());
             std::vector<double>::iterator result_min = std::min_element(lognormsig.begin(), lognormsig.end());
             double max_dist = *result;
             double min_dist = *result_min;

              //std::cout << "max " << *result << std::endl;
             const float eta = 5.0; // normalizing parameter.
             //log Normalize the vector values
             if((max_dist - min_dist) != 0)
             {
                 for(int p = 0 ; p < lognormsig.size() ; p++)
                 {
                     double normalized_val = log((((lognormsig.at(p) - min_dist)/(max_dist - min_dist)) * eta) + 1)/log(eta + 1) ;
                     lognormsig.at(p) = normalized_val;
                 }

             }

#ifdef LOG
                 printvec(lognormsig);
#endif

                 for(int j = 0; j < num_vertices(myARGA); j++)
                 {
                          //assign the normalized vector back
                          for(int k = 0; k < num_vertices(myARGB); k++)
                          {

                              //For a particular signature i ,get distance between Aj,Bk segment
                              Cost.at( j*num_vertices(myARGB) + k).at(i) = lognormsig.at(j*num_vertices(myARGB)+k);

                          }
                 }

                 lognormsig.clear();
        }



#ifdef LOG
         std::cout << " ========== Normalized Matrix Printing it ================ " << std::endl;
         printmat(Cost);
#endif

        for(int t = 0; t < num_vertices(myARGA) ; t++)
        {

            for(int u = 0; u < num_vertices(myARGB) ; u++)
            {

                double cumulative_dist = 0;

                //Get all the signature values for this segment
                for(int n = 0; n < NUM_SIGNATURE_USED; n++)
                {
                    cumulative_dist =  cumulative_dist + 10*Cost.at( t*num_vertices(myARGB) + u).at(n);

                }

                CostMat(t,u) = cumulative_dist;

            }

        }


        return CostMat;
}
#endif



//Function to compute cost matrix
void ComputeCostMatrixForARGS(AttrRelGraph myARG_groundtruth,AttrRelGraph myARG_testmesh)
{

#ifdef LOCAL_NORMALIZATION
    //Normalizes for a particular segment-signature
    ComputeDistanceForUnaryAttribute(myARG_groundtruth,myARG_testmesh);
#endif

#ifdef GLOBAL_LINEAR_NORMALIZATION
//Linearly Normalizes across all the segments for a particular signature
    ComputeDistanceForUnaryAttribute2(myARG_groundtruth,myARG_testmesh);
#endif

#ifdef GLOBAL_NON_LINEAR_NORMALIZATION
//Log Normalizes across all the segments for a particular signature
    ComputeDistanceForUnaryAttribute3(myARG_groundtruth,myARG_testmesh);
#endif

}

//Function to compute permutation matrix using Hungarian Algorithm
void ComputePermutationMatrixForARGS(Eigen::MatrixXf CostMat,Eigen::MatrixXd &PermMat,int rawPermMat[][MAX_SEGMENTS] )
{
#ifdef LOG
    std::cout <<"CostMat :: " << std::endl;
    std::cout << CostMat << std::endl;
#endif
    int rows,cols;

    rows = CostMat.rows();
    cols = CostMat.cols();
#ifdef LOG
    std::cout << "Cost Mat Rows  " << rows << "Cost Mat Cols  " << cols << std::endl;
#endif
    int CostArray[MAX_SEGMENTS*MAX_SEGMENTS];
    for(int i = 0; i < MAX_SEGMENTS*MAX_SEGMENTS; i++ )
    {
        CostArray[i] = 0;
    }

    int count = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            CostArray[count] = CostMat(i,j);
            count++;
        }

    }

    int out_PermMat[MAX_SEGMENTS][MAX_SEGMENTS];

    for(int i = 0; i < MAX_SEGMENTS; i++ )
    {
        for(int j = 0; j < MAX_SEGMENTS; j++ )
            out_PermMat[i][j] = 0;
    }

    solve_hungarian(CostArray,rows,cols,out_PermMat);

    for(int i = 0; i < MAX_SEGMENTS; i++ )
    {
        for(int j = 0; j < MAX_SEGMENTS; j++ )
             rawPermMat[i][j] = out_PermMat[i][j];
    }

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            PermMat(i,j) = out_PermMat[i][j];
        }

    }

#ifdef LOG
    std::cout << "PermMat Size" <<std::endl;
    std::cout << PermMat.rows() << " " << PermMat.cols() << std::endl;
    std::cout << PermMat;
#endif

}


void UpdatePolysegAssignment(AttrRelGraph &myARGA,AttrRelGraph &myARGB,int rawPermMat[][MAX_SEGMENTS])
{
    graph_traits<AttrRelGraph>::vertex_iterator viA, vi_endA;
    graph_traits<AttrRelGraph>::vertex_iterator viB, vi_endB;

    //Set Matching for GraphA
    for (boost::tie(viA ,vi_endA) = vertices(myARGA); viA != vi_endA; ++viA)
    {

        //Set Matching for GraphB
        for (boost::tie(viB ,vi_endB) = vertices(myARGB); viB != vi_endB; ++viB)
        {
            int indexA = myARGA[*viA].vert_index;
            int indexB = myARGB[*viB].vert_index;
            if(rawPermMat[indexA][indexB] == 1)
            {
                myARGA[*viA].IsMatched = true;
                myARGB[*viB].IsMatched = true;
                myARGA[*viA].matchedTo = myARGB[*viB].segment_name;
                myARGA[*viA].matchedToIndex = myARGB[*viB].vert_index;
                myARGB[*viB].matchedTo = myARGA[*viA].segment_name;
                myARGB[*viB].matchedToIndex = myARGA[*viA].vert_index;

            }


        }


    }


}



void GetNeighBour(boost::graph_traits<AttrRelGraph>::out_edge_iterator ei, const AttrRelGraph &myARG)
{
    graph_traits<AttrRelGraph>::vertex_descriptor nbr_vertex;
    nbr_vertex = target(*ei, myARG);
#ifdef LOG
    std::cout <<"Neighbouring Vertex Name " << myARG[nbr_vertex].segment_name;
    std::cout <<"Neighbouring Vertex Index  " << myARG[nbr_vertex].vert_index;
#endif
}

double ComputePartBasedMetric(const Eigen::MatrixXf &CostMat,const Eigen::MatrixXd &PermMat,int segment_diff)
{
    double metric = 0.0;
    double max_dist = 0.0;
    for(int i = 0; i < CostMat.rows(); i++)
    {
        for(int j = 0; j < CostMat.cols(); j++)
        {
            if(PermMat(i,j) == 1)
            {
                metric = metric + CostMat(i,j);
                if(CostMat(i,j) > max_dist)
                    max_dist = CostMat(i,j);
            }

        }

    }

    //Add penalty for non matched segments
    metric = metric + segment_diff*max_dist;

    return metric;
}



//compute sqrt(Sigma(i^2))
double ComputeImprovedPartBasedMetric(const Eigen::MatrixXf &CostMat,const Eigen::MatrixXd &PermMat,int segment_diff)
{
    double metric = 0.0;
    double max_dist = 0.0;
    for(int i = 0; i < CostMat.rows(); i++)
    {
        for(int j = 0; j < CostMat.cols(); j++)
        {
            if(PermMat(i,j) == 1)
            {
                metric = metric + CostMat(i,j)*CostMat(i,j);
                if(CostMat(i,j) > max_dist)
                    max_dist = CostMat(i,j);
            }

        }


    }


    //Add penalty for non matched segments
    metric = metric + (segment_diff*max_dist)*(segment_diff*max_dist);
    return sqrt(metric);
}

//Function to compute Metric
void ComputeSegmentMetric()
{




}


//Gound distance
float dist(feature_t *F1, feature_t *F2)
{
  float dX = F1->X - F2->X;

  if (dX < 0)
      dX = -dX;

  return dX;
}


//Based on http://robotics.stanford.edu/~rubner/emd/example1.c
float ComputeEMDForSegmentSignatures(std::vector<double> Sig1, std::vector<double> Sig2)
{

    feature_t f1[66],f2[66];// Change this in accordance to the size of the signature
    float w1[66], w2[66];

#ifdef DEBUG
    std::cout << "Sig1.size() = " << Sig1.size() << std::endl;
    std::cout << "Sig2.size() = " << Sig2.size() << std::endl;
#endif

    for(int i = 0; i < Sig1.size() ; i++)
    {
        f1[i].X = Sig1[i];
        f2[i].X = Sig2[i];
        w1[i] = 1;
        w2[i] = 1;
    }

    signature_t s1 = { 66, f1, w1}, s2 = { 66, f2, w2};

    float e;
    e = emd(&s1, &s2, dist, 0, 0);
    return e;
}



double ComputeEuclideanDistance(vector<double> Sig1, vector<double> Sig2)
{
    Eigen::VectorXd v(64);

    //Compute difference vector
    for(int i = 1; i <= 64  ; i++) //There are two buffers at the to ends in boost histogram
    {
        v[i] = Sig1[i] - Sig2[i];
    }

    //Compute Norm
    double e;
    e = v.norm();
    return e;
}


double ComputeEuclideanDistanceForEigenSig(vector<double> Sig1, vector<double> Sig2)
{
#ifdef LOG
    std::cout << "Sig1.size() " << Sig1.size() << std::endl ;
    std::cout << "Sig2.size() " << Sig2.size() << std::endl ;
#endif
    int vec_size = max(Sig1.size(),Sig2.size());

    Eigen::VectorXd v(vec_size);

    //Compute difference vector
    for(int i = 0; i < vec_size  ; i++)
    {
        v[i] = Sig1[i] - Sig2[i];
    }

    //Compute Norm
    double e;
    e = v.norm();
    return e;
}


void PrintSISignature(std::vector<double> hist, string name)
{
    ofstream myfile;
    stringstream filename;
    filename << name << "SI" << ".csv";
    const char * c = filename.str().c_str();
    myfile.open(c);
    for(int i = 0;i < hist.size() ; i++)
        myfile << hist[i] << "\n";

    myfile.close();
}


void PrintSDFSignature(std::vector<double> hist,string name)
{
    ofstream myfile;
    stringstream filename;
    filename << name << "SDF"  << ".csv";
    const char * c = filename.str().c_str();
    myfile.open(c);
    for(int i = 0;i < hist.size() ; i++)
        myfile << hist[i] << "\n";

    myfile.close();
}

//Test function for testing hungarian lib
void TestLibHungarian()
{
    int status;
    status = test_hungarian();
    std::cout << "Test Hungarian Status: " << status << std::endl;

}



void PrintMatching(AttrRelGraph myARG)
{
    std::cout << "==================== Matching  ===========================" << std::endl;

    graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;
    for (boost::tie(vi, vi_end) = vertices(myARG); vi != vi_end; ++vi)
    {
        std::cout << myARG[*vi].segment_name << " :: " << myARG[*vi].matchedTo << std::endl;

    }

    std::cout << "==========================================================" << std::endl;

}


void TestSegArg(AttrRelGraph myARG)
{

    std::cout << "==================== Unary Props ===========================" << std::endl;
    //Init Unary Props
    graph_traits<AttrRelGraph>::vertex_iterator vi, vi_end;
    for (boost::tie(vi, vi_end) = vertices(myARG); vi != vi_end; ++vi)
    {
        std::cout << "segment_name: " << myARG[*vi].segment_name << std::endl;
        std::cout << "segment_area_ratio: " << myARG[*vi].segment_area_ratio << std::endl;
        std::cout << "eigen_value_ratio: " << myARG[*vi].eigen_value_ratio << std::endl;
        std::cout << "eigen_shape: " << myARG[*vi].eigen_shape[0] << " " << myARG[*vi].eigen_shape[1] << " " << myARG[*vi].eigen_shape[2] << std::endl;
        std::cout << "vertex degree : " << out_degree(*vi, myARG) << std::endl;
        PrintSISignature(myARG[*vi].si_signature,myARG[*vi].segment_name);
        PrintSDFSignature(myARG[*vi].sdf_signature,myARG[*vi].segment_name);

    }

    int edge_count = 0;
    std::cout << "==================== Binary Props ===========================" << std::endl;
    //Init Binary Props
    graph_traits<AttrRelGraph>::edge_iterator ei, ei_end;
    for (boost::tie(ei, ei_end) = edges(myARG); ei != ei_end; ++ei)
    {
        std::cout << "Edge " << edge_count++ << std::endl;
        std::cout << myARG[*ei].segment_euclidean_distance << std::endl;
        std::cout << myARG[*ei].angle_between_max_eigenvector << std::endl;;
        std::cout << myARG[*ei].angle_between_second_max_eigenvector << std::endl;;
    }


}





/*

1. Find the Mesh with more number of segents
2. Find non-matched segments.
3. Find neighbours of non-matched segments.
4. Join the non-matched segment with one of neighnouring segment,which was matched
5. Find distance of merged segment with matched segment
6. If the distance of less than previous keep the merger.
7. Solve the segment matching again ? and compute the Distance ? Or Just compute the local distance and
and add it.

*/





bool CompareDistances(UnaryAttributes propsA,UnaryAttributes propsB,DistanceMeasures &dist)
{
    //Absolute distance between area ratios
    double segment_area_ratio_diff = abs(propsA.segment_area_ratio -  propsB.segment_area_ratio);
    dist.segment_area_ratio_diff = segment_area_ratio_diff;

    //EMD distance between SI Signatures
    float emd_si_sig = ComputeEMDForSegmentSignatures(propsA.si_signature,propsB.si_signature);
    dist.emd_si_sig = emd_si_sig;

    //EMD for SDF Signature of Segments
   // float emd_sdf_sig = ComputeEMDForSegmentSignatures(propsA.sdf_signature,propsB.sdf_signature);
    //dist.emd_sdf_sig = emd_sdf_sig;


    //Euclidean distance between eigen shape descriptor
    double eu_eigen_shape_descr = ComputeEuclideanDistanceForEigenSig(propsA.eigen_shape,propsB.eigen_shape);
    dist.eu_eigen_shape_descr = eu_eigen_shape_descr;

}
